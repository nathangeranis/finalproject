﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using HeroBoard.DATA.EF;
using Microsoft.AspNet.Identity;
using System.Globalization;
using System.Data.SqlTypes;

namespace HeroBoard.MVC.UI.Controllers
{
    [Authorize]
    public class ApplicationsController : Controller
    {
        private Entities db = new Entities();

        // GET: Applications
        [Authorize(Roles = "User, Manager, Admin")]
        public ActionResult Index()
        {
            string currentUserID = User.Identity.GetUserId();
            var applications = db.Applications.Include(a => a.AspNetUser).Include(a => a.OpenPosition);
            if (User.IsInRole("User"))
            {
                return View(applications.ToList().Where(a => a.UserId == currentUserID));

            }
            if (User.IsInRole("Manager"))
            {
                return View(applications.ToList().Where(a => a.OpenPosition.Location.ManagerID == currentUserID));

            }
            return View(applications.ToList());
        }

        // GET: Applications/Details/5
        [Authorize(Roles = "User, Manger, Admin")]
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Application application = db.Applications.Find(id);
            if (application == null)
            {
                return HttpNotFound();
            }
            return View(application);
        }

        // GET: Applications/Create
        [Authorize(Roles = "Admin")]
        public ActionResult Create()
        {
            ViewBag.UserId = new SelectList(db.AspNetUsers, "Id", "FirstName");
            ViewBag.OpenPositionID = new SelectList(db.OpenPositions, "OpenPositionID", "OpenPositionID");
            return View();
        }

        // POST: Applications/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Admin")]
        public ActionResult Create([Bind(Include = "ApplicationID,OpenPositionID,UserId,ApplicationDate,ManagerNotes,IsDeclined,ResumeFileName")] Application application)
        {
            if (ModelState.IsValid)
            {
                db.Applications.Add(application);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.UserId = new SelectList(db.AspNetUsers, "Id", "FirstName", application.UserId);
            ViewBag.OpenPositionID = new SelectList(db.OpenPositions, "OpenPositionID", "OpenPositionID", application.OpenPositionID);
            return View(application);
        }

        [Authorize(Roles = "User")]
        public ActionResult Apply(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            OpenPosition openP = db.OpenPositions.Find(id);
            if (openP == null)
            {
                return HttpNotFound();
            }

            string currentUserID = User.Identity.GetUserId();
            AspNetUser currentUser = db.AspNetUsers.Find(currentUserID);
            var apps = (from a in db.Applications
                        where a.OpenPositionID == id
                        orderby a.UserId
                        select a).ToList();

            if (apps != null)
            {
                foreach (Application a in apps)
                {
                    if (a.UserId.Contains(currentUserID))
                    {
                        TempData["message"] = "You've already applied for that position!";
                        return RedirectToAction("Index", "OpenPositions");
                    }
                }
            }
            if (apps != null)
            {
                if (currentUser.ResumeFileName != null)
                {
                    var userFirstName = currentUser.FirstName;
                    var userLastName = currentUser.LastName;

                    Application newApp = new Application();
                    newApp.OpenPositionID = id.Value;
                    newApp.UserId = currentUserID;
                    newApp.ApplicationDate = DateTime.Now.Date;

                    newApp.ResumeFileName = currentUser.ResumeFileName;
                    newApp.IsDeclined = null;
                    db.Applications.Add(newApp);
                    db.SaveChanges();
                    TempData["message"] = "Application Successful!";
                    return RedirectToAction("Index", "Applications");

                }
            }
            return RedirectToAction("Index", "Manage");
        }
        [Authorize(Roles = "Admin, Manager")]
        public ActionResult AppReject(int? id, bool b)
        {
            Application app = db.Applications.Find(id);
            app.IsDeclined = b;
            db.SaveChanges();
            return RedirectToAction("Index");
        }
        // GET: Applications/Edit/5
        [Authorize(Roles = "Admin, Manager")]
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Application application = db.Applications.Find(id);
            if (application == null)
            {
                return HttpNotFound();
            }
            ViewBag.UserId = new SelectList(db.AspNetUsers, "Id", "FirstName", application.UserId);
            ViewBag.OpenPositionID = new SelectList(db.OpenPositions, "OpenPositionID", "OpenPositionID", application.OpenPositionID);
            return View(application);
        }

        // POST: Applications/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Admin, Manager")]
        public ActionResult Edit([Bind(Include = "ApplicationID,OpenPositionID,UserId,ApplicationDate,ManagerNotes,IsDeclined,ResumeFileName")] Application application)
        {
            if (ModelState.IsValid)
            {
                db.Entry(application).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.UserId = new SelectList(db.AspNetUsers, "Id", "FirstName", application.UserId);
            ViewBag.OpenPositionID = new SelectList(db.OpenPositions, "OpenPositionID", "OpenPositionID", application.OpenPositionID);
            return View(application);
        }

        // GET: Applications/Delete/5
        [Authorize(Roles = "Admin, Manager")]
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Application application = db.Applications.Find(id);
            if (application == null)
            {
                return HttpNotFound();
            }
            return View(application);
        }

        // POST: Applications/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Admin, Manager")]
        public ActionResult DeleteConfirmed(int id)
        {
            Application application = db.Applications.Find(id);
            db.Applications.Remove(application);
            db.SaveChanges();
            return RedirectToAction("Index");
        }
        [Authorize(Roles = "User")]
        //public ActionResult Apply(int? id)
        //{
        //    string currentUserID = User.Identity.GetUserId();
        //    AspNetUser currentUser = 
        //}
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
