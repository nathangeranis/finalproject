﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using HeroBoard.DATA.EF;
using Microsoft.AspNet.Identity.Owin;
using HeroBoard.MVC.UI.Models;
using Microsoft.AspNet.Identity;
using System.IO;

namespace HeroBoard.MVC.UI.Controllers
{
    [Authorize]
    public class HeroesController : Controller
    {
        private Entities db = new Entities();

        private ApplicationUserManager _userManager;
        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }

        // GET: Heroes
        [AllowAnonymous]
        public ActionResult Index()
        {
            var heroes = db.Heroes.Include(h => h.AspNetUser).Include(h => h.Location).Include(h => h.SkillLevel);
            return View(heroes.ToList());
        }

        // GET: Heroes/Details/5
        [Authorize(Roles = "Manager, Admin")]
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Hero hero = db.Heroes.Find(id);
            if (hero == null)
            {
                return HttpNotFound();
            }
            return View(hero);
        }

        // GET: Heroes/Create
        [Authorize(Roles = "Admin")]
        public ActionResult Create()
        {
            ViewBag.UserID = new SelectList(db.AspNetUsers, "Id", "FirstName");
            ViewBag.LocationID = new SelectList(db.Locations, "LocationID", "City");
            ViewBag.SkillID = new SelectList(db.SkillLevels, "SkillID", "SkillName");
            return View();
        }

        // POST: Heroes/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Admin")]
        public ActionResult Create([Bind(Include = "HeroID,UserID,Alias,FirstName,LastName,Description,IsActive,SkillID,LocationID,ImageFileName")] Hero hero)
        {
            if (ModelState.IsValid)
            {
                db.Heroes.Add(hero);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.UserID = new SelectList(db.AspNetUsers, "Id", "FirstName", hero.UserID);
            ViewBag.LocationID = new SelectList(db.Locations, "LocationID", "City", hero.LocationID);
            ViewBag.SkillID = new SelectList(db.SkillLevels, "SkillID", "SkillName", hero.SkillID);
            return View(hero);
        }

        [HttpPost]
        //[ValidateAntiForgeryToken]
        [Authorize(Roles = "Admin, Manager")]
        public ActionResult HeroApplication(int? id, bool? b)
        {
            Application app = db.Applications.Find(id);
            Hero hero = new Hero();
            AspNetUser user = db.AspNetUsers.Find(app.UserId);
            app.IsDeclined = b;
            if (app.IsDeclined == false)
            {
                hero.Alias = "No Alias";
                hero.UserID = app.UserId;
                hero.FirstName = app.AspNetUser.FirstName;
                hero.LastName = app.AspNetUser.LastName;
                hero.Description = app.ManagerNotes;
                hero.ImageFileName = "noPhoto.png";
                hero.IsActive = true;
                hero.LocationID = app.OpenPosition.LocationID;
                hero.SkillID = 1;
                hero.Achievements = 0;
                if (ModelState.IsValid)
                {
                    var userManager = System.Web.HttpContext.
                        Current.GetOwinContext().GetUserManager<ApplicationUserManager>();
                    var roleAdd = UserManager.AddToRole(user.Id, "Hero");
                    var roleRem = UserManager.RemoveFromRole(user.Id, "User");

                    db.Heroes.Add(hero);
                    db.SaveChanges();
                    return RedirectToAction("Index");
                }

            }
            ViewBag.UserID = new SelectList(db.AspNetUsers, "Id", "FirstName", hero.UserID);
            ViewBag.LocationID = new SelectList(db.Locations, "LocationID", "City", hero.LocationID);
            ViewBag.SkillID = new SelectList(db.SkillLevels, "SkillID", "SkillName", hero.SkillID);
            return View(hero);
        }

        // GET: Heroes/Edit/5
        [Authorize(Roles = "Manager, Admin")]
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Hero hero = db.Heroes.Find(id);
            if (hero == null)
            {
                return HttpNotFound();
            }
            ViewBag.UserID = new SelectList(db.AspNetUsers, "Id", "FirstName", hero.UserID);
            ViewBag.LocationID = new SelectList(db.Locations, "LocationID", "City", hero.LocationID);
            ViewBag.SkillID = new SelectList(db.SkillLevels, "SkillID", "SkillName", hero.SkillID);
            return View(hero);
        }

        // POST: Heroes/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Manager, Admin")]
        public ActionResult Edit([Bind(Include = "HeroID,UserID,Alias,FirstName,LastName,Description,IsActive,SkillID,LocationID,ImageFileName,Achievements")] Hero hero, HttpPostedFileBase ImageName)
        {
            if (ModelState.IsValid)
            {
                string savePath = Server.MapPath("~/Content/img/");
                string deletePath = (hero.ImageFileName);
                string oldImageName = hero.ImageFileName;
                string imageName = "";
                string oldExt = "";
                if (oldImageName != null)
                {
                    oldExt = oldImageName.Substring(oldImageName.LastIndexOf('.'));
                }
                if (hero.ImageFileName != null)
                {
                    imageName = ImageName.FileName;
                    string ext = imageName.Substring(imageName.LastIndexOf('.')).ToLower();
                    imageName = Guid.NewGuid() + ext;
                    string[] goodExts = { ".jpg", ".jpeg", ".png" };
                    if (goodExts.Contains(ext))
                    {
                        if (ext == oldExt)
                        {
                            ImageName.SaveAs(savePath + imageName);
                            hero.ImageFileName = imageName;
                        }
                        else
                        {
                            FileInfo file = new FileInfo(deletePath);
                            if (file.Exists)
                            {
                                file.Delete();
                            }
                            ImageName.SaveAs(savePath + imageName);
                            hero.ImageFileName = imageName;
                        }
                    }
                    else
                    {
                        return View();
                    }
                }
                else
                {
                    string fileName = "noImage.jpeg";
                    fileName = ImageName.FileName;
                    string ext = Path.GetExtension(ImageName.FileName).ToLower();
                    fileName = Guid.NewGuid() + ext;
                    string[] goodExts = { ".jpg", ".jpeg", ".png" };
                    if (goodExts.Contains(ext))
                    {
                        ImageName.SaveAs(savePath + fileName);
                        hero.ImageFileName = fileName;
                    }
                    else
                    {
                        fileName = "noImage.png";
                        hero.ImageFileName = fileName;
                    }
                }
                db.Entry(hero).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.UserID = new SelectList(db.AspNetUsers, "Id", "FirstName", hero.UserID);
            ViewBag.LocationID = new SelectList(db.Locations, "LocationID", "City", hero.LocationID);
            ViewBag.SkillID = new SelectList(db.SkillLevels, "SkillID", "SkillName", hero.SkillID);
            return View(hero);
        }

        // GET: Heroes/Delete/5
        [Authorize(Roles = "Admin")]
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Hero hero = db.Heroes.Find(id);
            if (hero == null)
            {
                return HttpNotFound();
            }
            return View(hero);
        }

        // POST: Heroes/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Admin")]
        public ActionResult DeleteConfirmed(int id)
        {
            Hero hero = db.Heroes.Find(id);
            db.Heroes.Remove(hero);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
