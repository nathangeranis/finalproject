﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using HeroBoard.DATA.EF;
using Microsoft.AspNet.Identity;
using Newtonsoft.Json.Linq;
using System.Collections;

namespace HeroBoard.MVC.UI.Controllers
{
    [Authorize]
    public class AssignmentsController : Controller
    {
        private Entities db = new Entities();

        // GET: Assignments
        [Authorize(Roles = "Admin, Manager, Hero")]
        public ActionResult Index()
        {
            string currentUserID = User.Identity.GetUserId();
            var assignments = db.Assignments.Include(a => a.AspNetUser).Include(a => a.Hero).Include(a => a.Location).Include(a => a.OpenAssignment).Include(a => a.SkillLevel);
            if (User.IsInRole("Manager"))
            {
                return View(assignments.ToList().Where(a => a.ManagerID == currentUserID));

            }
            return View(assignments.ToList().Where(o => o.Hero.UserID == currentUserID));
        }

        // GET: Assignments/Details/5
        [Authorize(Roles = "Admin, Manager, Hero")]
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Assignment assignment = db.Assignments.Find(id);
            if (assignment == null)
            {
                return HttpNotFound();
            }
            return View(assignment);
        }

        // GET: Assignments/Create
        [Authorize(Roles = "Admin")]
        public ActionResult Create()
        {
            ViewBag.ManagerID = new SelectList(db.AspNetUsers, "Id", "FirstName");
            ViewBag.HeroID = new SelectList(db.Heroes, "HeroID", "UserID");
            ViewBag.LocationID = new SelectList(db.Locations, "LocationID", "City");
            ViewBag.OpenAssignID = new SelectList(db.OpenAssignments, "OpenAssignmentID", "ManagerID");
            ViewBag.SkillID = new SelectList(db.SkillLevels, "SkillID", "SkillName");
            return View();
        }

        // POST: Assignments/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Admin")]
        public ActionResult Create([Bind(Include = "AssignmentID,HeroID,ManagerID,LocationID,Description,SkillID,OpenAssignID,IsActive")] Assignment assignment)
        {
            if (ModelState.IsValid)
            {
                db.Assignments.Add(assignment);
                //OpenAssignment openA = new OpenAssignment();
                //openA.LocationID = assignment.LocationID;
                //openA.ManagerID = assignment.ManagerID;
                //openA.Description = assignment.Description;
                //openA.SkillID = assignment.SkillID;
                //db.OpenAssignments.Add(openA);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.ManagerID = new SelectList(db.AspNetUsers, "Id", "FirstName", assignment.ManagerID);
            ViewBag.HeroID = new SelectList(db.Heroes, "HeroID", "UserID", assignment.HeroID);
            ViewBag.LocationID = new SelectList(db.Locations, "LocationID", "City", assignment.LocationID);
            ViewBag.OpenAssignID = new SelectList(db.OpenAssignments, "OpenAssignID", "ManagerID", assignment.OpenAssignID);
            ViewBag.SkillID = new SelectList(db.SkillLevels, "SkillID", "SkillName", assignment.SkillID);
            return View(assignment);
        }
        [Authorize(Roles ="Hero")]
        public ActionResult Complete(int? id)
        {
            Assignment assign = db.Assignments.Find(id);
            string currentUserID = User.Identity.GetUserId();
            //AspNetUser currentUser = db.AspNetUsers.Find(currentUserID);
            var hero = db.Heroes.Where(a => a.UserID == currentUserID).FirstOrDefault();
            assign.IsActive = false;
            hero.Achievements++;
            var value = db.SkillLevels.Where(d => d.SkillID == hero.SkillLevel.SkillID +1).FirstOrDefault();
            if (hero.Achievements >= value.SkillValue)
            {
                hero.SkillID++;
                Session["Message"] = "You've achieved the Skill Level of " + hero.SkillLevel + "!";
            }
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        // GET: Assignments/Edit/5
        [Authorize(Roles = "Admin, Manager")]
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Assignment assignment = db.Assignments.Find(id);
            if (assignment == null)
            {
                return HttpNotFound();
            }
            ViewBag.ManagerID = new SelectList(db.AspNetUsers, "Id", "FirstName", assignment.ManagerID);
            ViewBag.HeroID = new SelectList(db.Heroes, "HeroID", "UserID", assignment.HeroID);
            ViewBag.LocationID = new SelectList(db.Locations, "LocationID", "City", assignment.LocationID);
            ViewBag.OpenAssignID = new SelectList(db.OpenAssignments, "OpenAssignID", "ManagerID", assignment.OpenAssignID);
            ViewBag.SkillID = new SelectList(db.SkillLevels, "SkillID", "SkillName", assignment.SkillID);
            return View(assignment);
        }

        // POST: Assignments/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Admin, Manager")]
        public ActionResult Edit([Bind(Include = "AssignmentID,HeroID,ManagerID,LocationID,Description,SkillID,OpenAssignID,IsActive")] Assignment assignment)
        {
            if (ModelState.IsValid)
            {
                db.Entry(assignment).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.ManagerID = new SelectList(db.AspNetUsers, "Id", "FirstName", assignment.ManagerID);
            ViewBag.HeroID = new SelectList(db.Heroes, "HeroID", "UserID", assignment.HeroID);
            ViewBag.LocationID = new SelectList(db.Locations, "LocationID", "City", assignment.LocationID);
            ViewBag.OpenAssignID = new SelectList(db.OpenAssignments, "OpenAssignID", "ManagerID", assignment.OpenAssignID);
            ViewBag.SkillID = new SelectList(db.SkillLevels, "SkillID", "SkillName", assignment.SkillID);
            return View(assignment);
        }

        // GET: Assignments/Delete/5
        [Authorize(Roles = "Admin, Manager")]
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Assignment assignment = db.Assignments.Find(id);
            if (assignment == null)
            {
                return HttpNotFound();
            }
            return View(assignment);
        }

        // POST: Assignments/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Admin, Manager")]
        public ActionResult DeleteConfirmed(int id)
        {
            Assignment assignment = db.Assignments.Find(id);
            db.Assignments.Remove(assignment);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
