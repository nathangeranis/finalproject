﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using HeroBoard.DATA.EF;
using Microsoft.AspNet.Identity;

namespace HeroBoard.MVC.UI.Controllers
{
    [Authorize]
    public class OpenAssignmentsController : Controller
    {
        private Entities db = new Entities();

        // GET: OpenAssignments
        [Authorize(Roles = "Admin, Manager, Hero")]
        public ActionResult Index()
        {
            string currentUserID = User.Identity.GetUserId();
            var hero = db.Heroes.Where(a => a.UserID == currentUserID).FirstOrDefault();
            var openAssignments = db.OpenAssignments.Include(o => o.AspNetUser).Include(o => o.Location).Include(o => o.SkillLevel);
            if (User.IsInRole("Manager"))
            {
                return View(openAssignments.ToList().Where(o => o.ManagerID == hero.Location.ManagerID));

            }
            return View(openAssignments.ToList());

        }
        // GET: OpenAssignments/Details/5
        [Authorize(Roles = "Admin, Manager, Hero")]
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            OpenAssignment openAssignment = db.OpenAssignments.Find(id);
            if (openAssignment == null)
            {
                return HttpNotFound();
            }
            return View(openAssignment);
        }

        // GET: OpenAssignments/Create
        [Authorize(Roles = "Admin, Manager")]
        public ActionResult Create()
        {
            string currentUserID = User.Identity.GetUserId();
            if (User.IsInRole("Manager"))
            {
                ViewBag.ManagerID = new SelectList(db.AspNetUsers.Where(a => a.Id == currentUserID), "Id", "FirstName");
                ViewBag.LocationID = new SelectList(db.Locations.Where(a => a.ManagerID == currentUserID), "LocationID", "City");
                ViewBag.SkillID = new SelectList(db.SkillLevels, "SkillID", "SkillName");
                return View();

            }
            ViewBag.ManagerID = new SelectList(db.AspNetUsers, "Id", "FirstName");
            ViewBag.LocationID = new SelectList(db.Locations, "LocationID", "City");
            ViewBag.SkillID = new SelectList(db.SkillLevels, "SkillID", "SkillName");
            return View();


        }

        // POST: OpenAssignments/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Admin, Manager")]
        public ActionResult Create([Bind(Include = "OpenAssignID,LocationID,ManagerID,Description,SkillID")] OpenAssignment openAssignment)
        {
            if (ModelState.IsValid)
            {
                db.OpenAssignments.Add(openAssignment);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.ManagerID = new SelectList(db.AspNetUsers, "Id", "FirstName", openAssignment.ManagerID);
            ViewBag.LocationID = new SelectList(db.Locations, "LocationID", "City", openAssignment.LocationID);
            ViewBag.SkillID = new SelectList(db.SkillLevels, "SkillID", "SkillName", openAssignment.SkillID);
            return View(openAssignment);
        }

        [Authorize(Roles = "Hero")]
        public ActionResult Apply(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            OpenAssignment openA = db.OpenAssignments.Find(id);
            if (openA == null)
            {
                return HttpNotFound();
            }

            string currentUserID = User.Identity.GetUserId();
            //AspNetUser currentUser = db.AspNetUsers.Find(currentUserID);
            var hero = db.Heroes.Where(a => a.UserID == currentUserID).FirstOrDefault();
            var assigns = (from a in db.Assignments
                           where a.OpenAssignID == id
                           select a).ToList();

            if (assigns != null)
            {
                foreach (Assignment a in assigns)
                {
                    if (a.OpenAssignID == openA.OpenAssignID && a.IsActive == true)
                    {
                        TempData["message"] = "You've already taken that task!";
                        return RedirectToAction("Index", "OpenAssignments");
                    }
                }
            }
            if (assigns != null)
            {
                if (hero.SkillID >= openA.SkillID)
                {
                    Assignment newAssign = new Assignment();
                    newAssign.OpenAssignID = openA.OpenAssignID;
                    newAssign.Description = openA.Description;
                    newAssign.IsActive = true;
                    newAssign.SkillID = openA.SkillID;
                    newAssign.LocationID = openA.LocationID;
                    newAssign.ManagerID = openA.ManagerID;
                    newAssign.HeroID = hero.HeroID;
                    db.Assignments.Add(newAssign);
                    db.SaveChanges();
                    TempData["message"] = "Assignment Taken!";
                    return RedirectToAction("Index", "Assignments");
                }
                TempData["message"] = "You do not meet the requirements for this Assignment!";
                return RedirectToAction("Index", "OpenAssignments");
            }
            return RedirectToAction("Index", "Manage");
        }
        // GET: OpenAssignments/Edit/5
        [Authorize(Roles = "Admin, Manager")]
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            OpenAssignment openAssignment = db.OpenAssignments.Find(id);
            if (openAssignment == null)
            {
                return HttpNotFound();
            }
            string currentUserID = User.Identity.GetUserId();
            ViewBag.ManagerID = new SelectList(db.AspNetUsers.Where(a => a.Id == currentUserID), "Id", "FirstName", openAssignment.ManagerID);
            ViewBag.LocationID = new SelectList(db.Locations.Where(a => a.ManagerID == currentUserID), "LocationID", "City", openAssignment.LocationID);
            ViewBag.SkillID = new SelectList(db.SkillLevels, "SkillID", "SkillName", openAssignment.SkillID);
            return View(openAssignment);
        }

        // POST: OpenAssignments/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Admin, Manager")]
        public ActionResult Edit([Bind(Include = "OpenAssignID,LocationID,ManagerID,Description,SkillID")] OpenAssignment openAssignment)
        {
            if (ModelState.IsValid)
            {
                db.Entry(openAssignment).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.ManagerID = new SelectList(db.AspNetUsers, "Id", "FirstName", openAssignment.ManagerID);
            ViewBag.LocationID = new SelectList(db.Locations, "LocationID", "City", openAssignment.LocationID);
            ViewBag.SkillID = new SelectList(db.SkillLevels, "SkillID", "SkillName", openAssignment.SkillID);
            return View(openAssignment);
        }

        // GET: OpenAssignments/Delete/5
        [Authorize(Roles = "Admin, Manager")]
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            OpenAssignment openAssignment = db.OpenAssignments.Find(id);
            if (openAssignment == null)
            {
                return HttpNotFound();
            }
            return View(openAssignment);
        }

        // POST: OpenAssignments/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Admin, Manager")]
        public ActionResult DeleteConfirmed(int id)
        {
            OpenAssignment openAssignment = db.OpenAssignments.Find(id);
            db.OpenAssignments.Remove(openAssignment);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
