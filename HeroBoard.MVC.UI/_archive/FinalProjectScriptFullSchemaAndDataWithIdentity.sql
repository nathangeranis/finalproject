IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_OpenPositions_Positions]') AND parent_object_id = OBJECT_ID(N'[dbo].[OpenPositions]'))
ALTER TABLE [dbo].[OpenPositions] DROP CONSTRAINT [FK_OpenPositions_Positions]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_OpenPositions_Location]') AND parent_object_id = OBJECT_ID(N'[dbo].[OpenPositions]'))
ALTER TABLE [dbo].[OpenPositions] DROP CONSTRAINT [FK_OpenPositions_Location]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_OpenAssignments_SkillLevel]') AND parent_object_id = OBJECT_ID(N'[dbo].[OpenAssignments]'))
ALTER TABLE [dbo].[OpenAssignments] DROP CONSTRAINT [FK_OpenAssignments_SkillLevel]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_OpenAssignments_Location]') AND parent_object_id = OBJECT_ID(N'[dbo].[OpenAssignments]'))
ALTER TABLE [dbo].[OpenAssignments] DROP CONSTRAINT [FK_OpenAssignments_Location]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_OpenAssignments_AspNetUsers]') AND parent_object_id = OBJECT_ID(N'[dbo].[OpenAssignments]'))
ALTER TABLE [dbo].[OpenAssignments] DROP CONSTRAINT [FK_OpenAssignments_AspNetUsers]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_AspNetUsers_Location]') AND parent_object_id = OBJECT_ID(N'[dbo].[Location]'))
ALTER TABLE [dbo].[Location] DROP CONSTRAINT [FK_AspNetUsers_Location]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Heroes_SkillLevel]') AND parent_object_id = OBJECT_ID(N'[dbo].[Heroes]'))
ALTER TABLE [dbo].[Heroes] DROP CONSTRAINT [FK_Heroes_SkillLevel]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Heroes_Location]') AND parent_object_id = OBJECT_ID(N'[dbo].[Heroes]'))
ALTER TABLE [dbo].[Heroes] DROP CONSTRAINT [FK_Heroes_Location]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Heroes_AspNetUsers]') AND parent_object_id = OBJECT_ID(N'[dbo].[Heroes]'))
ALTER TABLE [dbo].[Heroes] DROP CONSTRAINT [FK_Heroes_AspNetUsers]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Assignments_SkillLevel]') AND parent_object_id = OBJECT_ID(N'[dbo].[Assignments]'))
ALTER TABLE [dbo].[Assignments] DROP CONSTRAINT [FK_Assignments_SkillLevel]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Assignments_OpenAssignments]') AND parent_object_id = OBJECT_ID(N'[dbo].[Assignments]'))
ALTER TABLE [dbo].[Assignments] DROP CONSTRAINT [FK_Assignments_OpenAssignments]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Assignments_Location]') AND parent_object_id = OBJECT_ID(N'[dbo].[Assignments]'))
ALTER TABLE [dbo].[Assignments] DROP CONSTRAINT [FK_Assignments_Location]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Assignments_Heroes]') AND parent_object_id = OBJECT_ID(N'[dbo].[Assignments]'))
ALTER TABLE [dbo].[Assignments] DROP CONSTRAINT [FK_Assignments_Heroes]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Assignments_AspNetUsers]') AND parent_object_id = OBJECT_ID(N'[dbo].[Assignments]'))
ALTER TABLE [dbo].[Assignments] DROP CONSTRAINT [FK_Assignments_AspNetUsers]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_dbo.AspNetUserRoles_dbo.AspNetUsers_UserId]') AND parent_object_id = OBJECT_ID(N'[dbo].[AspNetUserRoles]'))
ALTER TABLE [dbo].[AspNetUserRoles] DROP CONSTRAINT [FK_dbo.AspNetUserRoles_dbo.AspNetUsers_UserId]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_dbo.AspNetUserRoles_dbo.AspNetRoles_RoleId]') AND parent_object_id = OBJECT_ID(N'[dbo].[AspNetUserRoles]'))
ALTER TABLE [dbo].[AspNetUserRoles] DROP CONSTRAINT [FK_dbo.AspNetUserRoles_dbo.AspNetRoles_RoleId]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_dbo.AspNetUserLogins_dbo.AspNetUsers_UserId]') AND parent_object_id = OBJECT_ID(N'[dbo].[AspNetUserLogins]'))
ALTER TABLE [dbo].[AspNetUserLogins] DROP CONSTRAINT [FK_dbo.AspNetUserLogins_dbo.AspNetUsers_UserId]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_dbo.AspNetUserClaims_dbo.AspNetUsers_UserId]') AND parent_object_id = OBJECT_ID(N'[dbo].[AspNetUserClaims]'))
ALTER TABLE [dbo].[AspNetUserClaims] DROP CONSTRAINT [FK_dbo.AspNetUserClaims_dbo.AspNetUsers_UserId]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Applications_OpenPositions]') AND parent_object_id = OBJECT_ID(N'[dbo].[Applications]'))
ALTER TABLE [dbo].[Applications] DROP CONSTRAINT [FK_Applications_OpenPositions]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Applications_AspNetUsers]') AND parent_object_id = OBJECT_ID(N'[dbo].[Applications]'))
ALTER TABLE [dbo].[Applications] DROP CONSTRAINT [FK_Applications_AspNetUsers]
GO
/****** Object:  Table [dbo].[SkillLevel]    Script Date: 4/4/2018 1:33:11 PM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SkillLevel]') AND type in (N'U'))
DROP TABLE [dbo].[SkillLevel]
GO
/****** Object:  Table [dbo].[Positions]    Script Date: 4/4/2018 1:33:11 PM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Positions]') AND type in (N'U'))
DROP TABLE [dbo].[Positions]
GO
/****** Object:  Table [dbo].[OpenPositions]    Script Date: 4/4/2018 1:33:11 PM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[OpenPositions]') AND type in (N'U'))
DROP TABLE [dbo].[OpenPositions]
GO
/****** Object:  Table [dbo].[OpenAssignments]    Script Date: 4/4/2018 1:33:11 PM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[OpenAssignments]') AND type in (N'U'))
DROP TABLE [dbo].[OpenAssignments]
GO
/****** Object:  Table [dbo].[Location]    Script Date: 4/4/2018 1:33:11 PM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Location]') AND type in (N'U'))
DROP TABLE [dbo].[Location]
GO
/****** Object:  Table [dbo].[Heroes]    Script Date: 4/4/2018 1:33:11 PM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Heroes]') AND type in (N'U'))
DROP TABLE [dbo].[Heroes]
GO
/****** Object:  Table [dbo].[Assignments]    Script Date: 4/4/2018 1:33:11 PM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Assignments]') AND type in (N'U'))
DROP TABLE [dbo].[Assignments]
GO
/****** Object:  Table [dbo].[AspNetUsers]    Script Date: 4/4/2018 1:33:11 PM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[AspNetUsers]') AND type in (N'U'))
DROP TABLE [dbo].[AspNetUsers]
GO
/****** Object:  Table [dbo].[AspNetUserRoles]    Script Date: 4/4/2018 1:33:11 PM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[AspNetUserRoles]') AND type in (N'U'))
DROP TABLE [dbo].[AspNetUserRoles]
GO
/****** Object:  Table [dbo].[AspNetUserLogins]    Script Date: 4/4/2018 1:33:11 PM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[AspNetUserLogins]') AND type in (N'U'))
DROP TABLE [dbo].[AspNetUserLogins]
GO
/****** Object:  Table [dbo].[AspNetUserClaims]    Script Date: 4/4/2018 1:33:11 PM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[AspNetUserClaims]') AND type in (N'U'))
DROP TABLE [dbo].[AspNetUserClaims]
GO
/****** Object:  Table [dbo].[AspNetRoles]    Script Date: 4/4/2018 1:33:11 PM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[AspNetRoles]') AND type in (N'U'))
DROP TABLE [dbo].[AspNetRoles]
GO
/****** Object:  Table [dbo].[Applications]    Script Date: 4/4/2018 1:33:11 PM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Applications]') AND type in (N'U'))
DROP TABLE [dbo].[Applications]
GO
/****** Object:  Table [dbo].[__MigrationHistory]    Script Date: 4/4/2018 1:33:11 PM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[__MigrationHistory]') AND type in (N'U'))
DROP TABLE [dbo].[__MigrationHistory]
GO
/****** Object:  Table [dbo].[__MigrationHistory]    Script Date: 4/4/2018 1:33:11 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[__MigrationHistory]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[__MigrationHistory](
	[MigrationId] [nvarchar](150) NOT NULL,
	[ContextKey] [nvarchar](300) NOT NULL,
	[Model] [varbinary](max) NOT NULL,
	[ProductVersion] [nvarchar](32) NOT NULL,
 CONSTRAINT [PK_dbo.__MigrationHistory] PRIMARY KEY CLUSTERED 
(
	[MigrationId] ASC,
	[ContextKey] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Applications]    Script Date: 4/4/2018 1:33:11 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Applications]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[Applications](
	[ApplicationID] [int] IDENTITY(1,1) NOT NULL,
	[OpenPositionID] [int] NOT NULL,
	[UserId] [nvarchar](128) NOT NULL,
	[ApplicationDate] [date] NOT NULL,
	[ManagerNotes] [varchar](2000) NULL,
	[IsDeclined] [bit] NULL,
	[ResumeFileName] [varchar](75) NULL,
 CONSTRAINT [PK_Applications] PRIMARY KEY CLUSTERED 
(
	[ApplicationID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[AspNetRoles]    Script Date: 4/4/2018 1:33:11 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[AspNetRoles]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[AspNetRoles](
	[Id] [nvarchar](128) NOT NULL,
	[Name] [nvarchar](256) NOT NULL,
 CONSTRAINT [PK_dbo.AspNetRoles] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[AspNetUserClaims]    Script Date: 4/4/2018 1:33:11 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[AspNetUserClaims]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[AspNetUserClaims](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[UserId] [nvarchar](128) NOT NULL,
	[ClaimType] [nvarchar](max) NULL,
	[ClaimValue] [nvarchar](max) NULL,
 CONSTRAINT [PK_dbo.AspNetUserClaims] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[AspNetUserLogins]    Script Date: 4/4/2018 1:33:11 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[AspNetUserLogins]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[AspNetUserLogins](
	[LoginProvider] [nvarchar](128) NOT NULL,
	[ProviderKey] [nvarchar](128) NOT NULL,
	[UserId] [nvarchar](128) NOT NULL,
 CONSTRAINT [PK_dbo.AspNetUserLogins] PRIMARY KEY CLUSTERED 
(
	[LoginProvider] ASC,
	[ProviderKey] ASC,
	[UserId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[AspNetUserRoles]    Script Date: 4/4/2018 1:33:11 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[AspNetUserRoles]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[AspNetUserRoles](
	[UserId] [nvarchar](128) NOT NULL,
	[RoleId] [nvarchar](128) NOT NULL,
 CONSTRAINT [PK_dbo.AspNetUserRoles] PRIMARY KEY CLUSTERED 
(
	[UserId] ASC,
	[RoleId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[AspNetUsers]    Script Date: 4/4/2018 1:33:11 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[AspNetUsers]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[AspNetUsers](
	[Id] [nvarchar](128) NOT NULL,
	[FirstName] [nvarchar](max) NULL,
	[LastName] [nvarchar](max) NULL,
	[ResumeFileName] [nvarchar](max) NULL,
	[Email] [nvarchar](256) NULL,
	[EmailConfirmed] [bit] NOT NULL,
	[PasswordHash] [nvarchar](max) NULL,
	[SecurityStamp] [nvarchar](max) NULL,
	[PhoneNumber] [nvarchar](max) NULL,
	[PhoneNumberConfirmed] [bit] NOT NULL,
	[TwoFactorEnabled] [bit] NOT NULL,
	[LockoutEndDateUtc] [datetime] NULL,
	[LockoutEnabled] [bit] NOT NULL,
	[AccessFailedCount] [int] NOT NULL,
	[UserName] [nvarchar](256) NOT NULL,
 CONSTRAINT [PK_dbo.AspNetUsers] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[Assignments]    Script Date: 4/4/2018 1:33:11 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Assignments]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[Assignments](
	[AssignmentID] [int] IDENTITY(1,1) NOT NULL,
	[HeroID] [int] NOT NULL,
	[ManagerID] [nvarchar](128) NOT NULL,
	[LocationID] [int] NOT NULL,
	[Description] [varchar](1000) NOT NULL,
	[SkillID] [int] NOT NULL,
	[OpenAssignID] [int] NOT NULL,
	[IsActive] [bit] NOT NULL,
 CONSTRAINT [PK_Assignments] PRIMARY KEY CLUSTERED 
(
	[AssignmentID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Heroes]    Script Date: 4/4/2018 1:33:11 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Heroes]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[Heroes](
	[HeroID] [int] IDENTITY(1,1) NOT NULL,
	[UserID] [nvarchar](128) NOT NULL,
	[Alias] [varchar](25) NULL,
	[FirstName] [varchar](25) NOT NULL,
	[LastName] [varchar](25) NOT NULL,
	[Description] [varchar](1000) NULL,
	[IsActive] [bit] NOT NULL,
	[SkillID] [int] NULL,
	[LocationID] [int] NOT NULL,
	[ImageFileName] [varchar](75) NULL,
	[Achievements] [int] NOT NULL,
 CONSTRAINT [PK_Heroes] PRIMARY KEY CLUSTERED 
(
	[HeroID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Location]    Script Date: 4/4/2018 1:33:11 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Location]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[Location](
	[LocationID] [int] IDENTITY(1,1) NOT NULL,
	[City] [varchar](50) NOT NULL,
	[State] [char](2) NULL,
	[Country] [varchar](25) NOT NULL,
	[ManagerID] [nvarchar](128) NULL,
 CONSTRAINT [PK_Location] PRIMARY KEY CLUSTERED 
(
	[LocationID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[OpenAssignments]    Script Date: 4/4/2018 1:33:11 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[OpenAssignments]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[OpenAssignments](
	[OpenAssignID] [int] IDENTITY(1,1) NOT NULL,
	[LocationID] [int] NOT NULL,
	[ManagerID] [nvarchar](128) NOT NULL,
	[Description] [varchar](1000) NOT NULL,
	[SkillID] [int] NOT NULL,
 CONSTRAINT [PK_OpenAssignments] PRIMARY KEY CLUSTERED 
(
	[OpenAssignID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[OpenPositions]    Script Date: 4/4/2018 1:33:11 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[OpenPositions]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[OpenPositions](
	[OpenPositionID] [int] IDENTITY(1,1) NOT NULL,
	[PositionID] [int] NOT NULL,
	[LocationID] [int] NOT NULL,
 CONSTRAINT [PK_OpenPositions] PRIMARY KEY CLUSTERED 
(
	[OpenPositionID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[Positions]    Script Date: 4/4/2018 1:33:11 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Positions]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[Positions](
	[PositionID] [int] IDENTITY(1,1) NOT NULL,
	[Title] [varchar](50) NOT NULL,
	[JobDescription] [varchar](max) NULL,
 CONSTRAINT [PK_Positions] PRIMARY KEY CLUSTERED 
(
	[PositionID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[SkillLevel]    Script Date: 4/4/2018 1:33:11 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SkillLevel]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[SkillLevel](
	[SkillID] [int] IDENTITY(1,1) NOT NULL,
	[SkillValue] [int] NOT NULL,
	[SkillName] [varchar](25) NOT NULL,
 CONSTRAINT [PK_SkillLevel] PRIMARY KEY CLUSTERED 
(
	[SkillID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
INSERT [dbo].[__MigrationHistory] ([MigrationId], [ContextKey], [Model], [ProductVersion]) VALUES (N'201803281848534_InitialCreate', N'HeroBoard.MVC.UI.Models.ApplicationDbContext', 0x1F8B0800000000000400DD5C5B6FE3B8157E2FD0FF20E8A92DB2562E9DC134B0779171E26ED0DC304E067D1BD012ED1023915A89CA2628F697ED437F52FF4249DD79D3C5962F592CB0189387DF393C3C240FA98FF9DFEFFF1DFFF41AF8D60B8C6244F0C43E191DDB16C42EF1105E4DEC842E7FF864FFF4E39FFF34BEF28257EB6B2177C6E5584B1C4FEC674AC373C789DD67188078142037223159D2914B020778C4393D3EFE877372E2400661332CCB1A7F493045014C7FB09F53825D18D204F8B7C4837E9C97B39A798A6ADD8100C62170E1C4FE1946E433019137BAFD3A1D3D5D8FB226B675E123C0CC99437F695B0063420165C69E3FC5704E238257F3901500FFF12D844C6E09FC18E69D38AFC4BBF6E7F894F7C7A91A16506E125312F4043C39CB1DE4C8CDD772B35D3A90B9F08AB99ABEF15EA76E9CD8D71E4C8BBE109F394056783EF5232E3CB16F4B1517717807E9A86838CA20671183FB9544DF4775C423AB73BBA332A04E47C7FCBF236B9AF83489E004C38446C03FB21E92858FDC7FC1B747F21DE2C9D9C96279F6E9C347E09D7DFC3B3CFB50EF29EB2B93130A58D14344421831DBE0B2ECBF6D39623B476E5836ABB5C9BCC26289CD0DDBBA05AF3710AFE8339B35A79F6C6B865EA15794E4C1F584119B4AAC118D12F6F32EF17DB0F06159EF34EAE4FF6FD07AFAE1E3205AEFC00B5AA5432FE967132762F3EA0BF4D3DAF81985D9F412C6FB5B2E368B48C07F8BF195D57E9B93247279678851E411442B4845EBC64E15BC9D429A430D1FD605EAE18736B7540D6FAD28EFD03A33A150B1EBD950D8BB5DBD9D23EE220CD9E0A5A1C53DD21470861D6B24411C59B2601544275D8308B3CEFD91D7C4198A62DAB230B27F7652DDACE906EC48D1171827019C211FEE44DD5500903FC0B6D2410B4BEB96280A6019279F099BC400F71EF50710C76C55F57E06F1F3D61D34876E12B1C93EA72008B7AEEDE199607897040BBE86EC4ED76043F3F82B99019792E80AF3561BE3DD10F73B49E815F62E01854FD42D00F9CF4714740718C49C0BD785713C63C10CBD2961A79602F01AD3B3D3DE707C9DDF775A37F5010AF4799DB4237D2B44ABDC4E2FA1E47706315D8ED764EA0D5921DCCDD442D46C6A26D16A6A2ED6D7540ED6CDD25CD26C682AD06A67263558D69C8ED0F069730A7BF879F366E98F692DA8B971CE5648F84F8861C49631EF01500A235C8D409775631FE9563A7C5CE9D6F7A654D357E02743AB5A6B36A48BC0F0B321853DFCD9909AC98A5F90C7B3920E87C94298C17792D79F53DBE79C64D9AEA783D0CD5D2BDFCD1A609A2E17714C5C94CE02CD35627E0924DACF7238ABFD4628EB8D7CABC43AC6021DF12D8F95B0BED97250DDE34BE8430AAD0B37BB669D82D8059EEA46D621AF8761C58EAA31ACBA5D128DFB9BA293453A8C7823C00F41319BA90853755A20ECA210F8AD5E925A76DCC278DF4B1D72CD250C21E60A5B3DD145B9FE32891B50EA9106A5CD4363A71671CD8168C85A4D63DE96C256E3AEDCF1EC24265B7267435CE6F9DB5602B3D9633B08CE66977431C07831BA8F00CDCF2A5D03403EB81C5A804A27264380E629D54E0254F4D81E025474C9BB0BD0EC88DA75FCA5F3EAA185A77850DEFDB6DEE8AE3DC4A6E08F030BCD2CF7646D286B0123353C2F17BC12BE52CDE18CD9999FCFE23CD595438483CF2115AF6CAA7C579B873ACD20721035015681D6029A7F5255809409D5C3B8E22EAFD1BA3C8BE8015BDCBB35C2E66BBF045B8B0115BBFE69B92668FE002D0767A7D347D9B3321A9420EF7458A8E16802425EBCC48E77708AE95E56754C975CB84F365CEB583E180D0E6AC95C0D4E2A3A33B8978AD06CF7922E21EB93926DE425297D3278A9E8CCE05ECA63B4DD499AA4A0475AB0918BC42D7CA0C956DC7494BB4D59377632DA595E30760CFCB4F12D0843845735BE5A5E62CD33B2DAF487797F02579061386EACE17195D6969A2889C00A4AB54C35B334FD5A7E092858007ECF33F502454CBBB71A96FF42657DFB5407B1D8070A69FEEFAC858908216CB86A469203CD5837039ED6A477E99A20D037B7388910F820D25CDF4F899F04D89C65995B671FF1EAEDB3121561EC48F62B5994E23225D715FDDF6974D49931E4489599CCFAA3658630F9BCC843EB5E37E5A66694E2AAAA8E62BABEDADBE899529AFE2326A78CFD07AC15613B33ACC6F4A983D48ABB63555C9E3A5455DA237A24B28E1045525D77D49C935307CB8B7A62D4681D0A58ADAE3BAAC8BCA9638A35DD11257A4D1D52AAEA61659D44231859AF580BCFE051BD44770D2A6DA68EAED6F6887595402304BD5ABD06B6C666B9AE3BAA86635307D65477C7AE0837F2AE71C0FBB5F1D0B6D9869D1DEE37DBB10D18DBD90286D9F06B1C863A50ADB82756CE5250C0F2F2830C29E30977B390CA2E76360B290386790D122800E212D4C85B30630ADFF58565BE89D760C6EB17B85B0D0FE5942B8B94DACBD3AE74AA1DE727CCF6A759CA913313B1ADC28D6C8B7F8B290C465C6034FFC59FFA08F205BD10B805182D614C332E8B7D7A7C722A3DEC3A9C47564E1C7BBEE6846E7A69258ED90E6869F80544EE33885492C8060F912A50E5FEFD1A7BF07562FF276D759E5EE5F07FA5C547D675FC84D12F09AB788C1268FDA6925E877998D17CBE3CD06734DDBD7AFDEF6F59D323EB3E6233E6DC3A967CB9CE088B8F6B7A599335DDC09AB59FDCBCDF09A5BC622990FF1280D7BFD6E1D679A9B21198FE35CA4690C28B13AD0FA5E9BFFE039305A2833C2ED9A8BFDA07241B216A1E890C8537880B4D8F40D6C1323E00F1D84F9A3E00E9393DB40F42D631CDF81804E1FE60F25390EE8B6ED1728F1BABE618B88B0538F5732B957E235EEDBE77628571BFD1445759F53DE03660CEAF1119EF8C743E582EA0E1940F86BDCFD0DE3A91FC50B8E315AB67BF94F15DB2C41BBEFFFDA1C8E1074067D4D0B3F64F01DF75AC99AEAE0F9C47DB8FE87D60C19693F6F64FE7DE75B0992EB50F3CD87A91B60F2CD6F6B57FEE39D23A6FA17BA760AB6C32C32728DDCD771BC53AFB4CC04EF80BC28220CB28B397B17A4E5F131FB945612562566A2613CA8A9589A3E855249AD5F6EB6BBEE13776369769566BA0E036E9CED7FF46DDB94CB36E03B1751FE4702DB55447D86F59C79AD86EEF890C2EF4A4E5ED415BCEDAC827784FDCEF419C22CC1EC317F1F743F51EC425434E9D1ED46EF5E336DB3B6B7F9D94EDDF315A5510FC6F9562E80ABB6629738D97A4D8BC258B0A11E986E61652E0B12DF522A268095CCAAAF91D73FAB43FBDB7E35F3A16D0BBC6F7090D13CABA0C83852F5C78F124A0497FCA5F176D1EDF87E95FA919A20BCC4CC4EFE6EFF1E704F95E69F74C73276480E0D9457EA3CBC792F29BDDD55B8974477047A0DC7D6552F40883D06760F13D9E8317B88E6D2CFC6EE00AB86FD50DA009A47D2044B78F2F1158452088738CAA3DFBC962D80B5E7FFC3FAF24B832A4570000, N'6.1.3-40302')
SET IDENTITY_INSERT [dbo].[Applications] ON 

INSERT [dbo].[Applications] ([ApplicationID], [OpenPositionID], [UserId], [ApplicationDate], [ManagerNotes], [IsDeclined], [ResumeFileName]) VALUES (49, 1, N'2507af9f-f2ef-4de0-90a7-405a9e6f4d7b', CAST(0x113E0B00 AS Date), NULL, 1, N'33bb4cb6-4e47-4b34-9b83-aa2149cd879d.pdf')
INSERT [dbo].[Applications] ([ApplicationID], [OpenPositionID], [UserId], [ApplicationDate], [ManagerNotes], [IsDeclined], [ResumeFileName]) VALUES (50, 10, N'7fce42d0-107d-4d68-8c2c-046f14615f41', CAST(0x113E0B00 AS Date), NULL, 1, N'19c0ca8c-06f1-4186-b07e-ab2e6a4bd81c.pdf')
SET IDENTITY_INSERT [dbo].[Applications] OFF
INSERT [dbo].[AspNetRoles] ([Id], [Name]) VALUES (N'd4f8836c-0a86-4ddc-adac-f2548db38610', N'Admin')
INSERT [dbo].[AspNetRoles] ([Id], [Name]) VALUES (N'ab2bc609-1276-49f2-818a-e44b2771043f', N'Hero')
INSERT [dbo].[AspNetRoles] ([Id], [Name]) VALUES (N'2121d39e-f170-4cf4-a812-e38cc23cb18a', N'Manager')
INSERT [dbo].[AspNetRoles] ([Id], [Name]) VALUES (N'f2910e84-d1c9-49b1-a2b1-48774998bb23', N'User')
INSERT [dbo].[AspNetUserRoles] ([UserId], [RoleId]) VALUES (N'6b6eb787-e951-4eec-9c6c-38c6625d12fc', N'2121d39e-f170-4cf4-a812-e38cc23cb18a')
INSERT [dbo].[AspNetUserRoles] ([UserId], [RoleId]) VALUES (N'76db0376-edc7-46d1-95b2-767519504215', N'2121d39e-f170-4cf4-a812-e38cc23cb18a')
INSERT [dbo].[AspNetUserRoles] ([UserId], [RoleId]) VALUES (N'f3a3ff46-d3ef-4e0a-bc36-ac25871fd755', N'2121d39e-f170-4cf4-a812-e38cc23cb18a')
INSERT [dbo].[AspNetUserRoles] ([UserId], [RoleId]) VALUES (N'0889b156-3fd1-4593-9bbc-8bc52a8fef8c', N'ab2bc609-1276-49f2-818a-e44b2771043f')
INSERT [dbo].[AspNetUserRoles] ([UserId], [RoleId]) VALUES (N'18584cc9-855b-459d-9e77-5699e30dbb98', N'ab2bc609-1276-49f2-818a-e44b2771043f')
INSERT [dbo].[AspNetUserRoles] ([UserId], [RoleId]) VALUES (N'2d9a34d0-2e65-4611-9f57-a58faab9af37', N'ab2bc609-1276-49f2-818a-e44b2771043f')
INSERT [dbo].[AspNetUserRoles] ([UserId], [RoleId]) VALUES (N'36ac166d-5869-42d1-91c6-1ddeb56555f6', N'ab2bc609-1276-49f2-818a-e44b2771043f')
INSERT [dbo].[AspNetUserRoles] ([UserId], [RoleId]) VALUES (N'41d8e19d-8715-45ea-a9aa-c6934be8c1a3', N'ab2bc609-1276-49f2-818a-e44b2771043f')
INSERT [dbo].[AspNetUserRoles] ([UserId], [RoleId]) VALUES (N'5dadc759-34d9-48ee-a130-0a7d7378aca2', N'ab2bc609-1276-49f2-818a-e44b2771043f')
INSERT [dbo].[AspNetUserRoles] ([UserId], [RoleId]) VALUES (N'67b8584d-ba47-459e-b46b-a9aa32344e71', N'ab2bc609-1276-49f2-818a-e44b2771043f')
INSERT [dbo].[AspNetUserRoles] ([UserId], [RoleId]) VALUES (N'6fed24f7-2cef-4813-abeb-223c68ec0b62', N'ab2bc609-1276-49f2-818a-e44b2771043f')
INSERT [dbo].[AspNetUserRoles] ([UserId], [RoleId]) VALUES (N'8e277094-d903-4f90-93f6-3bd006ed1734', N'ab2bc609-1276-49f2-818a-e44b2771043f')
INSERT [dbo].[AspNetUserRoles] ([UserId], [RoleId]) VALUES (N'9ba3340d-9bff-4823-88f8-feda0e2c1550', N'ab2bc609-1276-49f2-818a-e44b2771043f')
INSERT [dbo].[AspNetUserRoles] ([UserId], [RoleId]) VALUES (N'ad1f20a8-dc30-4b92-966b-ec508e3c697f', N'ab2bc609-1276-49f2-818a-e44b2771043f')
INSERT [dbo].[AspNetUserRoles] ([UserId], [RoleId]) VALUES (N'bba05399-6cce-4d48-8f92-e0fdaaa05ec1', N'ab2bc609-1276-49f2-818a-e44b2771043f')
INSERT [dbo].[AspNetUserRoles] ([UserId], [RoleId]) VALUES (N'ced270a6-d15f-4c01-b368-26ae88f50a6d', N'ab2bc609-1276-49f2-818a-e44b2771043f')
INSERT [dbo].[AspNetUserRoles] ([UserId], [RoleId]) VALUES (N'd45fec79-4a8a-402a-ade7-f034a4102a1f', N'ab2bc609-1276-49f2-818a-e44b2771043f')
INSERT [dbo].[AspNetUserRoles] ([UserId], [RoleId]) VALUES (N'd5945580-6b3e-4d50-9e54-101c44358091', N'ab2bc609-1276-49f2-818a-e44b2771043f')
INSERT [dbo].[AspNetUserRoles] ([UserId], [RoleId]) VALUES (N'3e537f2d-8fcf-421b-b86e-31a1bffcfc8a', N'd4f8836c-0a86-4ddc-adac-f2548db38610')
INSERT [dbo].[AspNetUserRoles] ([UserId], [RoleId]) VALUES (N'519ba4b6-d1ce-4103-8445-baa8534cdc99', N'd4f8836c-0a86-4ddc-adac-f2548db38610')
INSERT [dbo].[AspNetUserRoles] ([UserId], [RoleId]) VALUES (N'2507af9f-f2ef-4de0-90a7-405a9e6f4d7b', N'f2910e84-d1c9-49b1-a2b1-48774998bb23')
INSERT [dbo].[AspNetUserRoles] ([UserId], [RoleId]) VALUES (N'7fce42d0-107d-4d68-8c2c-046f14615f41', N'f2910e84-d1c9-49b1-a2b1-48774998bb23')
INSERT [dbo].[AspNetUsers] ([Id], [FirstName], [LastName], [ResumeFileName], [Email], [EmailConfirmed], [PasswordHash], [SecurityStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEndDateUtc], [LockoutEnabled], [AccessFailedCount], [UserName]) VALUES (N'0889b156-3fd1-4593-9bbc-8bc52a8fef8c', N'Tony', N'Stark', N'a772eb5f-d453-4c49-9991-779db03140b1.pdf', N'ironman@herocorp.com', 0, N'AAiCdWv0HeI90jz2SgdD6HRN5r1lkiOnHHxS32SyuoiOIY0xsvWT3x8TBNsymmrT4w==', N'c039c911-93e4-45f6-9750-d46352bc66cd', NULL, 0, 0, NULL, 1, 0, N'ironman@herocorp.com')
INSERT [dbo].[AspNetUsers] ([Id], [FirstName], [LastName], [ResumeFileName], [Email], [EmailConfirmed], [PasswordHash], [SecurityStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEndDateUtc], [LockoutEnabled], [AccessFailedCount], [UserName]) VALUES (N'18584cc9-855b-459d-9e77-5699e30dbb98', N'Austin', N'Hammell', N'84d6177a-27e1-4588-b9de-46d23461aefa.pdf', N'ahammell@herocorp.com', 0, N'AE1hrywQ2lk+24/zPDwG4t3rCjr2lVfQj6AVS70JyPj8EjbRjApXuxtHYU3uyDrJog==', N'587f1a3e-2ac7-4879-b56e-273bbdb19c3b', NULL, 0, 0, NULL, 1, 0, N'ahammell@herocorp.com')
INSERT [dbo].[AspNetUsers] ([Id], [FirstName], [LastName], [ResumeFileName], [Email], [EmailConfirmed], [PasswordHash], [SecurityStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEndDateUtc], [LockoutEnabled], [AccessFailedCount], [UserName]) VALUES (N'2507af9f-f2ef-4de0-90a7-405a9e6f4d7b', N'Wade', N'Wilson', N'33bb4cb6-4e47-4b34-9b83-aa2149cd879d.pdf', N'deadpool@herocorp.com', 0, N'AOv4/77b2hKMuhRNAQnndlrZVVgt21pD6YuDNgSclqHdVb5329IWxCHmk5RWYJ0aBA==', N'be7301c2-6a87-4152-baa6-2e26178d7252', NULL, 0, 0, NULL, 1, 0, N'deadpool@herocorp.com')
INSERT [dbo].[AspNetUsers] ([Id], [FirstName], [LastName], [ResumeFileName], [Email], [EmailConfirmed], [PasswordHash], [SecurityStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEndDateUtc], [LockoutEnabled], [AccessFailedCount], [UserName]) VALUES (N'2d9a34d0-2e65-4611-9f57-a58faab9af37', N'Scott', N'Lang', N'2a5c302b-0b85-4703-b7ff-f3802f89a9d9.pdf', N'antman@herocorp.com', 0, N'AJyu9/VawYRRvkkU05Rarm49eODkgu0nO5UHfq0IwtyNlVCAMST/MpmnxANRHt/dDw==', N'614c1793-c5bf-40f9-88cc-8037e02aefc9', NULL, 0, 0, NULL, 1, 0, N'antman@herocorp.com')
INSERT [dbo].[AspNetUsers] ([Id], [FirstName], [LastName], [ResumeFileName], [Email], [EmailConfirmed], [PasswordHash], [SecurityStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEndDateUtc], [LockoutEnabled], [AccessFailedCount], [UserName]) VALUES (N'36ac166d-5869-42d1-91c6-1ddeb56555f6', N'Logan', N'Howlett', N'fa021e10-54c4-4e7c-bd40-57a9398515e0.pdf', N'wolverine@herocorp.com', 0, N'AHueaDYnvOixhdq4ouhXsCQNKbzZQsGXHtbZEHAkOMiF0iG61VRsYxLaR6dzLcASgw==', N'de43bbea-8e7e-4906-909a-eee87d5566db', NULL, 0, 0, NULL, 1, 0, N'wolverine@herocorp.com')
INSERT [dbo].[AspNetUsers] ([Id], [FirstName], [LastName], [ResumeFileName], [Email], [EmailConfirmed], [PasswordHash], [SecurityStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEndDateUtc], [LockoutEnabled], [AccessFailedCount], [UserName]) VALUES (N'3e537f2d-8fcf-421b-b86e-31a1bffcfc8a', N'Super', N'Admin', NULL, N'superadmin@herocorp.com', 0, N'AMl0fJSxMCHBtO7w4dBRELeWJLUZO241OwDQrMH+27IQav4GCB4TVy1VQzJv99CjXQ==', N'46f016e0-7e27-497d-a5b3-c13579c2bbbd', NULL, 0, 0, NULL, 1, 0, N'superadmin@herocorp.com')
INSERT [dbo].[AspNetUsers] ([Id], [FirstName], [LastName], [ResumeFileName], [Email], [EmailConfirmed], [PasswordHash], [SecurityStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEndDateUtc], [LockoutEnabled], [AccessFailedCount], [UserName]) VALUES (N'41d8e19d-8715-45ea-a9aa-c6934be8c1a3', N'Bruce', N'Banner', N'3d5673fd-3eb3-4180-86a6-7bcc0d9778b2.pdf', N'hulk@herocorp.com', 0, N'APVQoAJNGmpubmI9joqMuhet7I66c5NwrSVxngf45FuOJFZ756bmAGWcEzNOr/ja0w==', N'15ce7a59-a93c-44ba-b814-7a600fde9e2a', NULL, 0, 0, NULL, 1, 0, N'hulk@herocorp.com')
INSERT [dbo].[AspNetUsers] ([Id], [FirstName], [LastName], [ResumeFileName], [Email], [EmailConfirmed], [PasswordHash], [SecurityStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEndDateUtc], [LockoutEnabled], [AccessFailedCount], [UserName]) VALUES (N'519ba4b6-d1ce-4103-8445-baa8534cdc99', N'Nathan', N'Geranis', N'C:\Users\Student\Documents\Visual Studio 2015\Projects\FinalProjectHeroBoard\src\HeroBoard.MVC.UI\Content\Resumes\Nathan Geranis Full Resume.pdf', N'nathangeranis@outlook.com', 1, N'AIzd1Sc1UzfejhgRhpuYQqFqtrIctL+ZLSmLaT0CVSy85uX4ZACQW7zqZbYmoT++9w==', N'e93adebd-5d8e-47a0-b5f7-96a91b771643', NULL, 0, 0, NULL, 1, 0, N'nathangeranis@outlook.com')
INSERT [dbo].[AspNetUsers] ([Id], [FirstName], [LastName], [ResumeFileName], [Email], [EmailConfirmed], [PasswordHash], [SecurityStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEndDateUtc], [LockoutEnabled], [AccessFailedCount], [UserName]) VALUES (N'5dadc759-34d9-48ee-a130-0a7d7378aca2', N'Natalia', N'Rominov', N'c685ed5d-8b2a-42ff-adb3-85e64bb26a9a.pdf', N'blackwidow@herocorp.com', 0, N'AO2llHNtPvkcXPH81lS/xoEeME1V8DDOOyCkQFfxqZ3kxRna+sm4F7KGaBHOI06unA==', N'103c76c6-d61e-4d96-955b-69aa164fbad3', NULL, 0, 0, NULL, 1, 0, N'blackwidow@herocorp.com')
INSERT [dbo].[AspNetUsers] ([Id], [FirstName], [LastName], [ResumeFileName], [Email], [EmailConfirmed], [PasswordHash], [SecurityStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEndDateUtc], [LockoutEnabled], [AccessFailedCount], [UserName]) VALUES (N'67b8584d-ba47-459e-b46b-a9aa32344e71', N'Wanda', N'Maximov', N'78bc34b1-d21e-4f84-8e22-5f3dcc2fc4b5.pdf', N'scarletwitch@herocorp.com', 0, N'AOYfs3PNuKUMFstWkKZwJQsm7kZGG7mBnkt3EOwXwYFQpdaekC3lkqRNMu9ubH+c1g==', N'630bbd27-83a7-4d66-b20a-4cf402fad66f', NULL, 0, 0, NULL, 1, 0, N'scarletwitch@herocorp.com')
INSERT [dbo].[AspNetUsers] ([Id], [FirstName], [LastName], [ResumeFileName], [Email], [EmailConfirmed], [PasswordHash], [SecurityStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEndDateUtc], [LockoutEnabled], [AccessFailedCount], [UserName]) VALUES (N'6b6eb787-e951-4eec-9c6c-38c6625d12fc', N'Phil', N'Coulson', NULL, N'manager2@herocorp.com', 0, N'AGMjb5KiUXCzNsxbTXeaXtumvXgoPZPsb5IlXsMAq/8D2bIXCJKNp+aH66tMKhZmSw==', N'63aa58dd-5924-4ef5-b013-bc978e72db18', NULL, 0, 0, NULL, 1, 0, N'manager2@herocorp.com')
INSERT [dbo].[AspNetUsers] ([Id], [FirstName], [LastName], [ResumeFileName], [Email], [EmailConfirmed], [PasswordHash], [SecurityStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEndDateUtc], [LockoutEnabled], [AccessFailedCount], [UserName]) VALUES (N'6fed24f7-2cef-4813-abeb-223c68ec0b62', N'Clint', N'Barton', N'42889f84-d50e-47e6-916a-e66f4c6a2b45.pdf', N'hawkeye@herocorp.com', 0, N'AJ8JuNNBYR/hYgjJ2Bw1srtwQqXPfQxLytJ92yoIqI747HyanQQ1PWKUShy7Fy4maQ==', N'b30b5dcf-cf1b-4c4f-92fd-e5c0b87fcaa0', NULL, 0, 0, NULL, 1, 0, N'hawkeye@herocorp.com')
INSERT [dbo].[AspNetUsers] ([Id], [FirstName], [LastName], [ResumeFileName], [Email], [EmailConfirmed], [PasswordHash], [SecurityStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEndDateUtc], [LockoutEnabled], [AccessFailedCount], [UserName]) VALUES (N'76db0376-edc7-46d1-95b2-767519504215', N'Mariah', N'Hill', NULL, N'manager3@herocorp.com', 0, N'ADK3GtWfouj5cd4KGgvEQulhM7y7AOfesHpRCTEKMN3HzE5g3MfBiqXdILxE33HE/Q==', N'8230257c-1df6-4894-9480-597cea5dcad8', NULL, 0, 0, NULL, 1, 0, N'manager3@herocorp.com')
INSERT [dbo].[AspNetUsers] ([Id], [FirstName], [LastName], [ResumeFileName], [Email], [EmailConfirmed], [PasswordHash], [SecurityStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEndDateUtc], [LockoutEnabled], [AccessFailedCount], [UserName]) VALUES (N'7fce42d0-107d-4d68-8c2c-046f14615f41', N'Batman', N'.', N'19c0ca8c-06f1-4186-b07e-ab2e6a4bd81c.pdf', N'batman@test.com', 0, N'AD+Kg7Fcek+g4PpJCVOt+GvVgRnZ7FrO5St1760LUgMaMeV5qcEmNberEGdL20i4GA==', N'b13f59f9-96b1-4c86-92b1-82916f06f90b', NULL, 0, 0, NULL, 1, 0, N'batman@test.com')
INSERT [dbo].[AspNetUsers] ([Id], [FirstName], [LastName], [ResumeFileName], [Email], [EmailConfirmed], [PasswordHash], [SecurityStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEndDateUtc], [LockoutEnabled], [AccessFailedCount], [UserName]) VALUES (N'8e277094-d903-4f90-93f6-3bd006ed1734', N'Steve', N'Rodgers', N'282728ce-1f86-4c3c-b3c4-45edb805ff0f.pdf', N'captainamerica@herocorp.com', 0, N'APrDIchPabE2fdzKBbj4mP8YpUroU8UPWp4GpgzXApriOAhW4Z0KqoXkyk5czZEclQ==', N'd124deb4-9d82-4a21-9c48-dee90ba9dba2', NULL, 0, 0, NULL, 1, 0, N'captainamerica@herocorp.com')
INSERT [dbo].[AspNetUsers] ([Id], [FirstName], [LastName], [ResumeFileName], [Email], [EmailConfirmed], [PasswordHash], [SecurityStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEndDateUtc], [LockoutEnabled], [AccessFailedCount], [UserName]) VALUES (N'9ba3340d-9bff-4823-88f8-feda0e2c1550', N'Peter', N'Parker', N'a781d126-ec13-4b73-b8b3-d40ae20a39ff.pdf', N'spiderman@herocorp.com', 0, N'AMOWY9ea0JmoA0ya/hjoM8mgtR0KX5abKskfSo3Zs130E9zoEG1myDvS6st+uA9eqA==', N'36d10a1f-f722-429d-876f-c2a4b318cb05', NULL, 0, 0, NULL, 1, 0, N'spiderman@herocorp.com')
INSERT [dbo].[AspNetUsers] ([Id], [FirstName], [LastName], [ResumeFileName], [Email], [EmailConfirmed], [PasswordHash], [SecurityStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEndDateUtc], [LockoutEnabled], [AccessFailedCount], [UserName]) VALUES (N'ad1f20a8-dc30-4b92-966b-ec508e3c697f', N'T', N'Challa', N'aab57859-7118-4e3e-828a-7a9c9f3fe01e.pdf', N'blackpanther@herocorp.com', 0, N'AGm4dHnC6wWboDD9R7wWI3AyxzSJyVQR7e+2esryDrUc9WrKfH2mISr+DJnJYPxwHw==', N'b6b4c26b-eb25-4bd5-a119-260a2451a368', NULL, 0, 0, NULL, 1, 0, N'blackpanther@herocorp.com')
INSERT [dbo].[AspNetUsers] ([Id], [FirstName], [LastName], [ResumeFileName], [Email], [EmailConfirmed], [PasswordHash], [SecurityStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEndDateUtc], [LockoutEnabled], [AccessFailedCount], [UserName]) VALUES (N'bba05399-6cce-4d48-8f92-e0fdaaa05ec1', N'Thor', N'Odinson', N'967b93ba-8dd0-4f75-aba9-33794f820b3b.pdf', N'thor@herocorp.com', 0, N'AK5gcPn0gKDSYW4qxv+VH/qVI5UQQBSnRBI8sNWp1KiXOjQzCIyZ1xmgh8NxorxC7g==', N'2ace4bc0-88b2-4a21-84ed-8127bc4eb6fd', NULL, 0, 0, NULL, 1, 0, N'thor@herocorp.com')
INSERT [dbo].[AspNetUsers] ([Id], [FirstName], [LastName], [ResumeFileName], [Email], [EmailConfirmed], [PasswordHash], [SecurityStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEndDateUtc], [LockoutEnabled], [AccessFailedCount], [UserName]) VALUES (N'ced270a6-d15f-4c01-b368-26ae88f50a6d', N'Brian', N'Hart', N'737465da-fcbc-4ace-a2d0-1ceb7484190f.pdf', N'bhart@herocorp.com', 0, N'ANoFTVX8lGQowEiRwL2gs8FObMMzZQflYa67arvPu5nFFmLsCs5crtaH/4zY4Yv43g==', N'b86d7426-9938-4300-aa2c-79d8c6e0ab6d', NULL, 0, 0, NULL, 1, 0, N'bhart@herocorp.com')
INSERT [dbo].[AspNetUsers] ([Id], [FirstName], [LastName], [ResumeFileName], [Email], [EmailConfirmed], [PasswordHash], [SecurityStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEndDateUtc], [LockoutEnabled], [AccessFailedCount], [UserName]) VALUES (N'd45fec79-4a8a-402a-ade7-f034a4102a1f', N'Ryan', N'White', N'8339ac50-ade6-4436-8bcb-3c43e68769d7.pdf', N'rwhite@herocorp.com', 0, N'AGnU9izA41PGjGFaxBdAUMUXG1tfBp17v9dKAhdokY+ENHuFVswb18t1sv/Ia4pV6Q==', N'55eaeea3-af82-4929-9e1b-92a44e8b1151', NULL, 0, 0, NULL, 1, 0, N'rwhite@herocorp.com')
INSERT [dbo].[AspNetUsers] ([Id], [FirstName], [LastName], [ResumeFileName], [Email], [EmailConfirmed], [PasswordHash], [SecurityStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEndDateUtc], [LockoutEnabled], [AccessFailedCount], [UserName]) VALUES (N'd5945580-6b3e-4d50-9e54-101c44358091', N'Jar', N'Vis', N'925a39d3-d666-422a-8ca4-20f845526f9d.pdf', N'vision@herocorp.com', 0, N'AGQvdLJy81EAuMrZqu8DK87T2VB3krPRzse7QTk1XXb2v+tkvQTFYrqi8xwQQ47PgQ==', N'91be0a4d-ca96-42ed-9130-ef668285426f', NULL, 0, 0, NULL, 1, 0, N'vision@herocorp.com')
INSERT [dbo].[AspNetUsers] ([Id], [FirstName], [LastName], [ResumeFileName], [Email], [EmailConfirmed], [PasswordHash], [SecurityStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEndDateUtc], [LockoutEnabled], [AccessFailedCount], [UserName]) VALUES (N'f3a3ff46-d3ef-4e0a-bc36-ac25871fd755', N'Nick', N'Fury', NULL, N'manager1@herocorp.com', 1, N'ABxvasbAjpxrV9P0xiYN1fUfvxuHk1EjbRj4+LN1zPbsceBeYs4aes7RjK2aY5YhqQ==', N'78140c74-d7bf-4a7a-b7de-33a6d98f2518', NULL, 0, 0, NULL, 1, 0, N'manager1@herocorp.com')
SET IDENTITY_INSERT [dbo].[Assignments] ON 

INSERT [dbo].[Assignments] ([AssignmentID], [HeroID], [ManagerID], [LocationID], [Description], [SkillID], [OpenAssignID], [IsActive]) VALUES (1, 7, N'f3a3ff46-d3ef-4e0a-bc36-ac25871fd755', 3, N'Cat Stuck in a Tree', 1, 2, 1)
INSERT [dbo].[Assignments] ([AssignmentID], [HeroID], [ManagerID], [LocationID], [Description], [SkillID], [OpenAssignID], [IsActive]) VALUES (2, 7, N'f3a3ff46-d3ef-4e0a-bc36-ac25871fd755', 5, N'Chitauri Invasion.', 5, 6, 1)
INSERT [dbo].[Assignments] ([AssignmentID], [HeroID], [ManagerID], [LocationID], [Description], [SkillID], [OpenAssignID], [IsActive]) VALUES (3, 16, N'76db0376-edc7-46d1-95b2-767519504215', 1, N'Jewelry store robbery in downtown LA.', 2, 12, 0)
INSERT [dbo].[Assignments] ([AssignmentID], [HeroID], [ManagerID], [LocationID], [Description], [SkillID], [OpenAssignID], [IsActive]) VALUES (5, 8, N'f3a3ff46-d3ef-4e0a-bc36-ac25871fd755', 3, N'Building on Fire at 55th and Roosevelt', 3, 4, 0)
INSERT [dbo].[Assignments] ([AssignmentID], [HeroID], [ManagerID], [LocationID], [Description], [SkillID], [OpenAssignID], [IsActive]) VALUES (6, 8, N'f3a3ff46-d3ef-4e0a-bc36-ac25871fd755', 7, N'Burglary on 11th and Main', 2, 3, 0)
INSERT [dbo].[Assignments] ([AssignmentID], [HeroID], [ManagerID], [LocationID], [Description], [SkillID], [OpenAssignID], [IsActive]) VALUES (7, 8, N'f3a3ff46-d3ef-4e0a-bc36-ac25871fd755', 3, N'Building on Fire at 55th and Roosevelt', 3, 4, 0)
INSERT [dbo].[Assignments] ([AssignmentID], [HeroID], [ManagerID], [LocationID], [Description], [SkillID], [OpenAssignID], [IsActive]) VALUES (8, 8, N'f3a3ff46-d3ef-4e0a-bc36-ac25871fd755', 3, N'Building on Fire at 55th and Roosevelt', 3, 4, 0)
INSERT [dbo].[Assignments] ([AssignmentID], [HeroID], [ManagerID], [LocationID], [Description], [SkillID], [OpenAssignID], [IsActive]) VALUES (9, 8, N'f3a3ff46-d3ef-4e0a-bc36-ac25871fd755', 7, N'Burglary on 11th and Main', 2, 3, 0)
SET IDENTITY_INSERT [dbo].[Assignments] OFF
SET IDENTITY_INSERT [dbo].[Heroes] ON 

INSERT [dbo].[Heroes] ([HeroID], [UserID], [Alias], [FirstName], [LastName], [Description], [IsActive], [SkillID], [LocationID], [ImageFileName], [Achievements]) VALUES (4, N'41d8e19d-8715-45ea-a9aa-c6934be8c1a3', N'The Incredible Hulk', N'Bruce', N'Banner', N'The Hulk possesses an incredible level of superhuman physical ability. His capacity for physical strength is potentially limitless due to the fact that the Hulk''s strength increases proportionally with his level of great emotional stress, anger in particular.', 1, 5, 5, N'9e6081b3-f513-437b-ade7-b14945bc9965.png', 25)
INSERT [dbo].[Heroes] ([HeroID], [UserID], [Alias], [FirstName], [LastName], [Description], [IsActive], [SkillID], [LocationID], [ImageFileName], [Achievements]) VALUES (5, N'2d9a34d0-2e65-4611-9f57-a58faab9af37', N'Ant-Man', N'Scott', N'Lang', N'Ant-Man carried a supply of Pym Particles in his belt, allowing him to reduce himself to the size of an ant, approximately one-half inch in height, by means of a rare group of sub-atomic particles.', 1, 5, 3, N'0c251334-260f-4f12-bb8b-5beb65ed74b1.png', 25)
INSERT [dbo].[Heroes] ([HeroID], [UserID], [Alias], [FirstName], [LastName], [Description], [IsActive], [SkillID], [LocationID], [ImageFileName], [Achievements]) VALUES (6, N'd5945580-6b3e-4d50-9e54-101c44358091', N'Vision', N'Jar', N'Vis', N'Vision is a sapient construct, a perfect hybrid between organic and inorganic material. His entire body is a mix between a synthetic simulacrum of organic tissue and Vibranium, all of which are enhanced by the cosmic powers of the Mind Stone to function as a living body.', 1, 5, 9, N'531384fa-013b-4b04-a278-57c9bd6424ed.png', 25)
INSERT [dbo].[Heroes] ([HeroID], [UserID], [Alias], [FirstName], [LastName], [Description], [IsActive], [SkillID], [LocationID], [ImageFileName], [Achievements]) VALUES (7, N'0889b156-3fd1-4593-9bbc-8bc52a8fef8c', N'Iron Man', N'Tony', N'Stark', N'The Iron Man armor includes Tony’s primary energy weapon, repulsor rays, a powerful particle beam which is standard equipment in the palms of his armor; the repulsor ray can repel physical and energy-based attacks, traveling as either a single stream or as a wide-field dispersal.', 1, 5, 5, N'47c50acf-7e28-4df0-9a8a-8f3d2abdea93.png', 25)
INSERT [dbo].[Heroes] ([HeroID], [UserID], [Alias], [FirstName], [LastName], [Description], [IsActive], [SkillID], [LocationID], [ImageFileName], [Achievements]) VALUES (8, N'9ba3340d-9bff-4823-88f8-feda0e2c1550', N'Spider Man', N'Peter', N'Parker', N'Can cling to most surfaces, has superhuman strength (able to lift 10 tons optimally) and is roughly 15 times more agile than a regular human. The combination of his acrobatic leaps and web-slinging enables him to travel rapidly from place to place.', 1, 4, 5, N'ee4fdead-f950-4ac3-8eb0-6e9e449f5f69.png', 19)
INSERT [dbo].[Heroes] ([HeroID], [UserID], [Alias], [FirstName], [LastName], [Description], [IsActive], [SkillID], [LocationID], [ImageFileName], [Achievements]) VALUES (9, N'8e277094-d903-4f90-93f6-3bd006ed1734', N'Captain America', N'Steve', N'Rodgers', N'Captain America represented the pinnacle of human physical perfection. He experienced a time when he was augmented to superhuman levels, but generally performed just below superhuman levels for most of his career.', 1, 5, 5, N'9ae4256e-06c3-4b6c-aff3-bc4fd053c66e.png', 25)
INSERT [dbo].[Heroes] ([HeroID], [UserID], [Alias], [FirstName], [LastName], [Description], [IsActive], [SkillID], [LocationID], [ImageFileName], [Achievements]) VALUES (10, N'5dadc759-34d9-48ee-a130-0a7d7378aca2', N'Black Widow', N'Natalia', N'Rominov', N'Government treatments have slowed her aging, augmented her immune system and enhanced her physical durability.', 1, 5, 7, N'4f6867ca-caf5-4e8f-ae30-de06b79cb2f3.png', 25)
INSERT [dbo].[Heroes] ([HeroID], [UserID], [Alias], [FirstName], [LastName], [Description], [IsActive], [SkillID], [LocationID], [ImageFileName], [Achievements]) VALUES (11, N'bba05399-6cce-4d48-8f92-e0fdaaa05ec1', N'Thor', N'Thor', N'Odinson', N'As the son of Odin and Gaea, Thor''s strength, endurance and resistance to injury are greater than the vast majority of his superhuman race. He is extremely long-lived (though not completely immune to aging), immune to conventional disease and highly resistant to injury. His flesh and bones are several times denser than a human''s.', 1, 5, 7, N'8ea42990-9f61-4fd7-bec1-77c3296ef45e.png', 25)
INSERT [dbo].[Heroes] ([HeroID], [UserID], [Alias], [FirstName], [LastName], [Description], [IsActive], [SkillID], [LocationID], [ImageFileName], [Achievements]) VALUES (12, N'ad1f20a8-dc30-4b92-966b-ec508e3c697f', N'Black Panther', N'T', N'Challa', N'T''Challa is a brilliant tactician, strategist, scientist, tracker and a master of all forms of unarmed combat whose unique hybrid fighting style incorporates acrobatics and aspects of animal mimicry. ', 1, 5, 11, N'970f1f35-ffbd-45d4-8346-54eedf895aec.png', 25)
INSERT [dbo].[Heroes] ([HeroID], [UserID], [Alias], [FirstName], [LastName], [Description], [IsActive], [SkillID], [LocationID], [ImageFileName], [Achievements]) VALUES (13, N'67b8584d-ba47-459e-b46b-a9aa32344e71', N'Scarlet Witch', N'Wanda', N'Maximov', N'The Scarlet Witch can tap into mystic energy for reality-altering effects; this power was formerly limited to the creation of "hex-spheres" of reality-disrupting quasi-psionic force to cause molecular disturbances in a target''s probability field, resulting in spontaneous combustion, deflection of objects in flight, and so on.', 1, 5, 8, N'aeee23b7-e4cd-447a-bac1-c24e1e628322.png', 25)
INSERT [dbo].[Heroes] ([HeroID], [UserID], [Alias], [FirstName], [LastName], [Description], [IsActive], [SkillID], [LocationID], [ImageFileName], [Achievements]) VALUES (14, N'36ac166d-5869-42d1-91c6-1ddeb56555f6', N'Wolverine', N'Logan', N'Howlett', N'Wolverine is a mutant who possesses the ability to regenerate damaged or destroyed areas of his cellular structure at a rate far greater than that of an ordinary human. The speed at which this healing factor works varies in direct proportion with the severity of the damage Wolverine suffers.', 1, 5, 10, N'2b7a657d-0d12-41ef-b5b9-df4e39460856.png', 25)
INSERT [dbo].[Heroes] ([HeroID], [UserID], [Alias], [FirstName], [LastName], [Description], [IsActive], [SkillID], [LocationID], [ImageFileName], [Achievements]) VALUES (15, N'6fed24f7-2cef-4813-abeb-223c68ec0b62', N'Hawkeye', N'Clint', N'Barton', N'Hawkeye is a world-class archer and marksman. His above average reflexes and hand-eye-coordination make him the most proficient archer ever known. He is also trained to throw knifes, darts, balls, bolas and boomerangs. ', 1, 5, 2, N'07002606-a0da-4558-b32f-186a3c2a3ee6.png', 25)
INSERT [dbo].[Heroes] ([HeroID], [UserID], [Alias], [FirstName], [LastName], [Description], [IsActive], [SkillID], [LocationID], [ImageFileName], [Achievements]) VALUES (16, N'18584cc9-855b-459d-9e77-5699e30dbb98', N'The Kid', N'Austin', N'Hammell', N'Programmer kid. Fresh outta high school. Got dem skills on a keyboard.', 1, 2, 4, N'21e67567-a079-4139-8b45-02696c7f868d.png', 3)
INSERT [dbo].[Heroes] ([HeroID], [UserID], [Alias], [FirstName], [LastName], [Description], [IsActive], [SkillID], [LocationID], [ImageFileName], [Achievements]) VALUES (17, N'ced270a6-d15f-4c01-b368-26ae88f50a6d', N'Mr. Moneybags', N'Brian', N'Hart', N'Reaches into pockets, pulls out money, problems go away.', 1, 2, 4, N'ff5cb25a-a4dc-4b13-942d-aa7d234489aa.png', 3)
SET IDENTITY_INSERT [dbo].[Heroes] OFF
SET IDENTITY_INSERT [dbo].[Location] ON 

INSERT [dbo].[Location] ([LocationID], [City], [State], [Country], [ManagerID]) VALUES (1, N'Los Angeles', N'CA', N'United States', N'76db0376-edc7-46d1-95b2-767519504215')
INSERT [dbo].[Location] ([LocationID], [City], [State], [Country], [ManagerID]) VALUES (2, N'Seattle', N'WA', N'United States', N'76db0376-edc7-46d1-95b2-767519504215')
INSERT [dbo].[Location] ([LocationID], [City], [State], [Country], [ManagerID]) VALUES (3, N'Miami', N'FL', N'United States', N'f3a3ff46-d3ef-4e0a-bc36-ac25871fd755')
INSERT [dbo].[Location] ([LocationID], [City], [State], [Country], [ManagerID]) VALUES (4, N'Kansas City', N'MO', N'United States', N'76db0376-edc7-46d1-95b2-767519504215')
INSERT [dbo].[Location] ([LocationID], [City], [State], [Country], [ManagerID]) VALUES (5, N'New York City', N'NY', N'United States', N'f3a3ff46-d3ef-4e0a-bc36-ac25871fd755')
INSERT [dbo].[Location] ([LocationID], [City], [State], [Country], [ManagerID]) VALUES (6, N'Paris', NULL, N'France', N'f3a3ff46-d3ef-4e0a-bc36-ac25871fd755')
INSERT [dbo].[Location] ([LocationID], [City], [State], [Country], [ManagerID]) VALUES (7, N'Munich', NULL, N'Germany', N'f3a3ff46-d3ef-4e0a-bc36-ac25871fd755')
INSERT [dbo].[Location] ([LocationID], [City], [State], [Country], [ManagerID]) VALUES (8, N'Stanislav', NULL, N'Sokovia', N'6b6eb787-e951-4eec-9c6c-38c6625d12fc')
INSERT [dbo].[Location] ([LocationID], [City], [State], [Country], [ManagerID]) VALUES (9, N'London', NULL, N'Great Britain', N'6b6eb787-e951-4eec-9c6c-38c6625d12fc')
INSERT [dbo].[Location] ([LocationID], [City], [State], [Country], [ManagerID]) VALUES (10, N'Tokyo', NULL, N'Japan', N'6b6eb787-e951-4eec-9c6c-38c6625d12fc')
INSERT [dbo].[Location] ([LocationID], [City], [State], [Country], [ManagerID]) VALUES (11, N'Birnin Zanda', NULL, N'Wakanda', N'6b6eb787-e951-4eec-9c6c-38c6625d12fc')
SET IDENTITY_INSERT [dbo].[Location] OFF
SET IDENTITY_INSERT [dbo].[OpenAssignments] ON 

INSERT [dbo].[OpenAssignments] ([OpenAssignID], [LocationID], [ManagerID], [Description], [SkillID]) VALUES (2, 3, N'f3a3ff46-d3ef-4e0a-bc36-ac25871fd755', N'Cat Stuck in a Tree', 1)
INSERT [dbo].[OpenAssignments] ([OpenAssignID], [LocationID], [ManagerID], [Description], [SkillID]) VALUES (3, 7, N'f3a3ff46-d3ef-4e0a-bc36-ac25871fd755', N'Burglary on 11th and Main', 2)
INSERT [dbo].[OpenAssignments] ([OpenAssignID], [LocationID], [ManagerID], [Description], [SkillID]) VALUES (4, 3, N'f3a3ff46-d3ef-4e0a-bc36-ac25871fd755', N'Building on Fire at 55th and Roosevelt', 3)
INSERT [dbo].[OpenAssignments] ([OpenAssignID], [LocationID], [ManagerID], [Description], [SkillID]) VALUES (5, 6, N'f3a3ff46-d3ef-4e0a-bc36-ac25871fd755', N'Terrorist Attack near the Eiffel Tower', 4)
INSERT [dbo].[OpenAssignments] ([OpenAssignID], [LocationID], [ManagerID], [Description], [SkillID]) VALUES (6, 5, N'f3a3ff46-d3ef-4e0a-bc36-ac25871fd755', N'Chitauri Invasion.', 5)
INSERT [dbo].[OpenAssignments] ([OpenAssignID], [LocationID], [ManagerID], [Description], [SkillID]) VALUES (7, 8, N'6b6eb787-e951-4eec-9c6c-38c6625d12fc', N'Ultron on a rampage. Must be stopped.', 5)
INSERT [dbo].[OpenAssignments] ([OpenAssignID], [LocationID], [ManagerID], [Description], [SkillID]) VALUES (8, 9, N'6b6eb787-e951-4eec-9c6c-38c6625d12fc', N'Cat Stuck in a Tree', 1)
INSERT [dbo].[OpenAssignments] ([OpenAssignID], [LocationID], [ManagerID], [Description], [SkillID]) VALUES (9, 10, N'6b6eb787-e951-4eec-9c6c-38c6625d12fc', N'Burglary on 11th and Main', 2)
INSERT [dbo].[OpenAssignments] ([OpenAssignID], [LocationID], [ManagerID], [Description], [SkillID]) VALUES (10, 11, N'6b6eb787-e951-4eec-9c6c-38c6625d12fc', N'Thanos inbound. Call to all active Heroes. Proceed with caution. Death Likely. 4/27/2018', 6)
INSERT [dbo].[OpenAssignments] ([OpenAssignID], [LocationID], [ManagerID], [Description], [SkillID]) VALUES (11, 9, N'6b6eb787-e951-4eec-9c6c-38c6625d12fc', N'Terrorist Attack near Big Ben.', 4)
INSERT [dbo].[OpenAssignments] ([OpenAssignID], [LocationID], [ManagerID], [Description], [SkillID]) VALUES (12, 1, N'76db0376-edc7-46d1-95b2-767519504215', N'Jewelry store robbery in downtown LA.', 2)
INSERT [dbo].[OpenAssignments] ([OpenAssignID], [LocationID], [ManagerID], [Description], [SkillID]) VALUES (13, 2, N'76db0376-edc7-46d1-95b2-767519504215', N'Space Needle threatened by extremist group', 4)
INSERT [dbo].[OpenAssignments] ([OpenAssignID], [LocationID], [ManagerID], [Description], [SkillID]) VALUES (14, 4, N'76db0376-edc7-46d1-95b2-767519504215', N'Awesome web application in need of development.', 3)
SET IDENTITY_INSERT [dbo].[OpenAssignments] OFF
SET IDENTITY_INSERT [dbo].[OpenPositions] ON 

INSERT [dbo].[OpenPositions] ([OpenPositionID], [PositionID], [LocationID]) VALUES (1, 1, 1)
INSERT [dbo].[OpenPositions] ([OpenPositionID], [PositionID], [LocationID]) VALUES (2, 2, 1)
INSERT [dbo].[OpenPositions] ([OpenPositionID], [PositionID], [LocationID]) VALUES (3, 1, 2)
INSERT [dbo].[OpenPositions] ([OpenPositionID], [PositionID], [LocationID]) VALUES (4, 2, 2)
INSERT [dbo].[OpenPositions] ([OpenPositionID], [PositionID], [LocationID]) VALUES (5, 1, 3)
INSERT [dbo].[OpenPositions] ([OpenPositionID], [PositionID], [LocationID]) VALUES (6, 2, 3)
INSERT [dbo].[OpenPositions] ([OpenPositionID], [PositionID], [LocationID]) VALUES (7, 1, 4)
INSERT [dbo].[OpenPositions] ([OpenPositionID], [PositionID], [LocationID]) VALUES (8, 2, 4)
INSERT [dbo].[OpenPositions] ([OpenPositionID], [PositionID], [LocationID]) VALUES (9, 1, 5)
INSERT [dbo].[OpenPositions] ([OpenPositionID], [PositionID], [LocationID]) VALUES (10, 2, 5)
INSERT [dbo].[OpenPositions] ([OpenPositionID], [PositionID], [LocationID]) VALUES (11, 1, 6)
INSERT [dbo].[OpenPositions] ([OpenPositionID], [PositionID], [LocationID]) VALUES (12, 2, 6)
INSERT [dbo].[OpenPositions] ([OpenPositionID], [PositionID], [LocationID]) VALUES (13, 1, 7)
INSERT [dbo].[OpenPositions] ([OpenPositionID], [PositionID], [LocationID]) VALUES (14, 2, 7)
INSERT [dbo].[OpenPositions] ([OpenPositionID], [PositionID], [LocationID]) VALUES (15, 1, 8)
INSERT [dbo].[OpenPositions] ([OpenPositionID], [PositionID], [LocationID]) VALUES (16, 2, 8)
INSERT [dbo].[OpenPositions] ([OpenPositionID], [PositionID], [LocationID]) VALUES (17, 1, 9)
INSERT [dbo].[OpenPositions] ([OpenPositionID], [PositionID], [LocationID]) VALUES (18, 2, 9)
INSERT [dbo].[OpenPositions] ([OpenPositionID], [PositionID], [LocationID]) VALUES (19, 1, 10)
INSERT [dbo].[OpenPositions] ([OpenPositionID], [PositionID], [LocationID]) VALUES (20, 2, 10)
INSERT [dbo].[OpenPositions] ([OpenPositionID], [PositionID], [LocationID]) VALUES (21, 1, 11)
INSERT [dbo].[OpenPositions] ([OpenPositionID], [PositionID], [LocationID]) VALUES (22, 2, 11)
SET IDENTITY_INSERT [dbo].[OpenPositions] OFF
SET IDENTITY_INSERT [dbo].[Positions] ON 

INSERT [dbo].[Positions] ([PositionID], [Title], [JobDescription]) VALUES (1, N'Hero', N'Being a hero isn''t an easy task. Apply with caution and good intention.')
INSERT [dbo].[Positions] ([PositionID], [Title], [JobDescription]) VALUES (2, N'Team Manager', N'Managing a team of heroes is a daunting task, yet so fulfilling.')
SET IDENTITY_INSERT [dbo].[Positions] OFF
SET IDENTITY_INSERT [dbo].[SkillLevel] ON 

INSERT [dbo].[SkillLevel] ([SkillID], [SkillValue], [SkillName]) VALUES (1, 0, N'Beginner')
INSERT [dbo].[SkillLevel] ([SkillID], [SkillValue], [SkillName]) VALUES (2, 3, N'Intermediate')
INSERT [dbo].[SkillLevel] ([SkillID], [SkillValue], [SkillName]) VALUES (3, 6, N'Advanced')
INSERT [dbo].[SkillLevel] ([SkillID], [SkillValue], [SkillName]) VALUES (4, 12, N'Expert')
INSERT [dbo].[SkillLevel] ([SkillID], [SkillValue], [SkillName]) VALUES (5, 20, N'Avenger')
INSERT [dbo].[SkillLevel] ([SkillID], [SkillValue], [SkillName]) VALUES (6, 100, N'Impossible')
SET IDENTITY_INSERT [dbo].[SkillLevel] OFF
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Applications_AspNetUsers]') AND parent_object_id = OBJECT_ID(N'[dbo].[Applications]'))
ALTER TABLE [dbo].[Applications]  WITH CHECK ADD  CONSTRAINT [FK_Applications_AspNetUsers] FOREIGN KEY([UserId])
REFERENCES [dbo].[AspNetUsers] ([Id])
ON DELETE CASCADE
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Applications_AspNetUsers]') AND parent_object_id = OBJECT_ID(N'[dbo].[Applications]'))
ALTER TABLE [dbo].[Applications] CHECK CONSTRAINT [FK_Applications_AspNetUsers]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Applications_OpenPositions]') AND parent_object_id = OBJECT_ID(N'[dbo].[Applications]'))
ALTER TABLE [dbo].[Applications]  WITH CHECK ADD  CONSTRAINT [FK_Applications_OpenPositions] FOREIGN KEY([OpenPositionID])
REFERENCES [dbo].[OpenPositions] ([OpenPositionID])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Applications_OpenPositions]') AND parent_object_id = OBJECT_ID(N'[dbo].[Applications]'))
ALTER TABLE [dbo].[Applications] CHECK CONSTRAINT [FK_Applications_OpenPositions]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_dbo.AspNetUserClaims_dbo.AspNetUsers_UserId]') AND parent_object_id = OBJECT_ID(N'[dbo].[AspNetUserClaims]'))
ALTER TABLE [dbo].[AspNetUserClaims]  WITH CHECK ADD  CONSTRAINT [FK_dbo.AspNetUserClaims_dbo.AspNetUsers_UserId] FOREIGN KEY([UserId])
REFERENCES [dbo].[AspNetUsers] ([Id])
ON DELETE CASCADE
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_dbo.AspNetUserClaims_dbo.AspNetUsers_UserId]') AND parent_object_id = OBJECT_ID(N'[dbo].[AspNetUserClaims]'))
ALTER TABLE [dbo].[AspNetUserClaims] CHECK CONSTRAINT [FK_dbo.AspNetUserClaims_dbo.AspNetUsers_UserId]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_dbo.AspNetUserLogins_dbo.AspNetUsers_UserId]') AND parent_object_id = OBJECT_ID(N'[dbo].[AspNetUserLogins]'))
ALTER TABLE [dbo].[AspNetUserLogins]  WITH CHECK ADD  CONSTRAINT [FK_dbo.AspNetUserLogins_dbo.AspNetUsers_UserId] FOREIGN KEY([UserId])
REFERENCES [dbo].[AspNetUsers] ([Id])
ON DELETE CASCADE
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_dbo.AspNetUserLogins_dbo.AspNetUsers_UserId]') AND parent_object_id = OBJECT_ID(N'[dbo].[AspNetUserLogins]'))
ALTER TABLE [dbo].[AspNetUserLogins] CHECK CONSTRAINT [FK_dbo.AspNetUserLogins_dbo.AspNetUsers_UserId]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_dbo.AspNetUserRoles_dbo.AspNetRoles_RoleId]') AND parent_object_id = OBJECT_ID(N'[dbo].[AspNetUserRoles]'))
ALTER TABLE [dbo].[AspNetUserRoles]  WITH CHECK ADD  CONSTRAINT [FK_dbo.AspNetUserRoles_dbo.AspNetRoles_RoleId] FOREIGN KEY([RoleId])
REFERENCES [dbo].[AspNetRoles] ([Id])
ON DELETE CASCADE
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_dbo.AspNetUserRoles_dbo.AspNetRoles_RoleId]') AND parent_object_id = OBJECT_ID(N'[dbo].[AspNetUserRoles]'))
ALTER TABLE [dbo].[AspNetUserRoles] CHECK CONSTRAINT [FK_dbo.AspNetUserRoles_dbo.AspNetRoles_RoleId]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_dbo.AspNetUserRoles_dbo.AspNetUsers_UserId]') AND parent_object_id = OBJECT_ID(N'[dbo].[AspNetUserRoles]'))
ALTER TABLE [dbo].[AspNetUserRoles]  WITH CHECK ADD  CONSTRAINT [FK_dbo.AspNetUserRoles_dbo.AspNetUsers_UserId] FOREIGN KEY([UserId])
REFERENCES [dbo].[AspNetUsers] ([Id])
ON DELETE CASCADE
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_dbo.AspNetUserRoles_dbo.AspNetUsers_UserId]') AND parent_object_id = OBJECT_ID(N'[dbo].[AspNetUserRoles]'))
ALTER TABLE [dbo].[AspNetUserRoles] CHECK CONSTRAINT [FK_dbo.AspNetUserRoles_dbo.AspNetUsers_UserId]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Assignments_AspNetUsers]') AND parent_object_id = OBJECT_ID(N'[dbo].[Assignments]'))
ALTER TABLE [dbo].[Assignments]  WITH CHECK ADD  CONSTRAINT [FK_Assignments_AspNetUsers] FOREIGN KEY([ManagerID])
REFERENCES [dbo].[AspNetUsers] ([Id])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Assignments_AspNetUsers]') AND parent_object_id = OBJECT_ID(N'[dbo].[Assignments]'))
ALTER TABLE [dbo].[Assignments] CHECK CONSTRAINT [FK_Assignments_AspNetUsers]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Assignments_Heroes]') AND parent_object_id = OBJECT_ID(N'[dbo].[Assignments]'))
ALTER TABLE [dbo].[Assignments]  WITH CHECK ADD  CONSTRAINT [FK_Assignments_Heroes] FOREIGN KEY([HeroID])
REFERENCES [dbo].[Heroes] ([HeroID])
ON DELETE CASCADE
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Assignments_Heroes]') AND parent_object_id = OBJECT_ID(N'[dbo].[Assignments]'))
ALTER TABLE [dbo].[Assignments] CHECK CONSTRAINT [FK_Assignments_Heroes]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Assignments_Location]') AND parent_object_id = OBJECT_ID(N'[dbo].[Assignments]'))
ALTER TABLE [dbo].[Assignments]  WITH CHECK ADD  CONSTRAINT [FK_Assignments_Location] FOREIGN KEY([LocationID])
REFERENCES [dbo].[Location] ([LocationID])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Assignments_Location]') AND parent_object_id = OBJECT_ID(N'[dbo].[Assignments]'))
ALTER TABLE [dbo].[Assignments] CHECK CONSTRAINT [FK_Assignments_Location]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Assignments_OpenAssignments]') AND parent_object_id = OBJECT_ID(N'[dbo].[Assignments]'))
ALTER TABLE [dbo].[Assignments]  WITH CHECK ADD  CONSTRAINT [FK_Assignments_OpenAssignments] FOREIGN KEY([OpenAssignID])
REFERENCES [dbo].[OpenAssignments] ([OpenAssignID])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Assignments_OpenAssignments]') AND parent_object_id = OBJECT_ID(N'[dbo].[Assignments]'))
ALTER TABLE [dbo].[Assignments] CHECK CONSTRAINT [FK_Assignments_OpenAssignments]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Assignments_SkillLevel]') AND parent_object_id = OBJECT_ID(N'[dbo].[Assignments]'))
ALTER TABLE [dbo].[Assignments]  WITH CHECK ADD  CONSTRAINT [FK_Assignments_SkillLevel] FOREIGN KEY([SkillID])
REFERENCES [dbo].[SkillLevel] ([SkillID])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Assignments_SkillLevel]') AND parent_object_id = OBJECT_ID(N'[dbo].[Assignments]'))
ALTER TABLE [dbo].[Assignments] CHECK CONSTRAINT [FK_Assignments_SkillLevel]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Heroes_AspNetUsers]') AND parent_object_id = OBJECT_ID(N'[dbo].[Heroes]'))
ALTER TABLE [dbo].[Heroes]  WITH CHECK ADD  CONSTRAINT [FK_Heroes_AspNetUsers] FOREIGN KEY([UserID])
REFERENCES [dbo].[AspNetUsers] ([Id])
ON DELETE CASCADE
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Heroes_AspNetUsers]') AND parent_object_id = OBJECT_ID(N'[dbo].[Heroes]'))
ALTER TABLE [dbo].[Heroes] CHECK CONSTRAINT [FK_Heroes_AspNetUsers]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Heroes_Location]') AND parent_object_id = OBJECT_ID(N'[dbo].[Heroes]'))
ALTER TABLE [dbo].[Heroes]  WITH CHECK ADD  CONSTRAINT [FK_Heroes_Location] FOREIGN KEY([LocationID])
REFERENCES [dbo].[Location] ([LocationID])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Heroes_Location]') AND parent_object_id = OBJECT_ID(N'[dbo].[Heroes]'))
ALTER TABLE [dbo].[Heroes] CHECK CONSTRAINT [FK_Heroes_Location]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Heroes_SkillLevel]') AND parent_object_id = OBJECT_ID(N'[dbo].[Heroes]'))
ALTER TABLE [dbo].[Heroes]  WITH CHECK ADD  CONSTRAINT [FK_Heroes_SkillLevel] FOREIGN KEY([SkillID])
REFERENCES [dbo].[SkillLevel] ([SkillID])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Heroes_SkillLevel]') AND parent_object_id = OBJECT_ID(N'[dbo].[Heroes]'))
ALTER TABLE [dbo].[Heroes] CHECK CONSTRAINT [FK_Heroes_SkillLevel]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_AspNetUsers_Location]') AND parent_object_id = OBJECT_ID(N'[dbo].[Location]'))
ALTER TABLE [dbo].[Location]  WITH CHECK ADD  CONSTRAINT [FK_AspNetUsers_Location] FOREIGN KEY([ManagerID])
REFERENCES [dbo].[AspNetUsers] ([Id])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_AspNetUsers_Location]') AND parent_object_id = OBJECT_ID(N'[dbo].[Location]'))
ALTER TABLE [dbo].[Location] CHECK CONSTRAINT [FK_AspNetUsers_Location]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_OpenAssignments_AspNetUsers]') AND parent_object_id = OBJECT_ID(N'[dbo].[OpenAssignments]'))
ALTER TABLE [dbo].[OpenAssignments]  WITH CHECK ADD  CONSTRAINT [FK_OpenAssignments_AspNetUsers] FOREIGN KEY([ManagerID])
REFERENCES [dbo].[AspNetUsers] ([Id])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_OpenAssignments_AspNetUsers]') AND parent_object_id = OBJECT_ID(N'[dbo].[OpenAssignments]'))
ALTER TABLE [dbo].[OpenAssignments] CHECK CONSTRAINT [FK_OpenAssignments_AspNetUsers]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_OpenAssignments_Location]') AND parent_object_id = OBJECT_ID(N'[dbo].[OpenAssignments]'))
ALTER TABLE [dbo].[OpenAssignments]  WITH CHECK ADD  CONSTRAINT [FK_OpenAssignments_Location] FOREIGN KEY([LocationID])
REFERENCES [dbo].[Location] ([LocationID])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_OpenAssignments_Location]') AND parent_object_id = OBJECT_ID(N'[dbo].[OpenAssignments]'))
ALTER TABLE [dbo].[OpenAssignments] CHECK CONSTRAINT [FK_OpenAssignments_Location]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_OpenAssignments_SkillLevel]') AND parent_object_id = OBJECT_ID(N'[dbo].[OpenAssignments]'))
ALTER TABLE [dbo].[OpenAssignments]  WITH CHECK ADD  CONSTRAINT [FK_OpenAssignments_SkillLevel] FOREIGN KEY([SkillID])
REFERENCES [dbo].[SkillLevel] ([SkillID])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_OpenAssignments_SkillLevel]') AND parent_object_id = OBJECT_ID(N'[dbo].[OpenAssignments]'))
ALTER TABLE [dbo].[OpenAssignments] CHECK CONSTRAINT [FK_OpenAssignments_SkillLevel]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_OpenPositions_Location]') AND parent_object_id = OBJECT_ID(N'[dbo].[OpenPositions]'))
ALTER TABLE [dbo].[OpenPositions]  WITH CHECK ADD  CONSTRAINT [FK_OpenPositions_Location] FOREIGN KEY([LocationID])
REFERENCES [dbo].[Location] ([LocationID])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_OpenPositions_Location]') AND parent_object_id = OBJECT_ID(N'[dbo].[OpenPositions]'))
ALTER TABLE [dbo].[OpenPositions] CHECK CONSTRAINT [FK_OpenPositions_Location]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_OpenPositions_Positions]') AND parent_object_id = OBJECT_ID(N'[dbo].[OpenPositions]'))
ALTER TABLE [dbo].[OpenPositions]  WITH CHECK ADD  CONSTRAINT [FK_OpenPositions_Positions] FOREIGN KEY([PositionID])
REFERENCES [dbo].[Positions] ([PositionID])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_OpenPositions_Positions]') AND parent_object_id = OBJECT_ID(N'[dbo].[OpenPositions]'))
ALTER TABLE [dbo].[OpenPositions] CHECK CONSTRAINT [FK_OpenPositions_Positions]
GO
