﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HeroBoard.DATA.EF
{
    public class METADATA
    {
    }
    public class ApplicationMETADATA
    {
        [DisplayFormat(DataFormatString = "{0:d}")]
        [Display(Name = "Application Date")]
        public System.DateTime ApplicationDate { get; set; }

        [DisplayFormat(NullDisplayText = "N/A")]
        [UIHint("MultilineText")]
        [Display(Name = "Notes")]
        public string ManagerNotes { get; set; }

        [Display(Name = "Decline?")]
        public Nullable<bool> IsDeclined { get; set; }

        [Display(Name = "Resume")]
        public string ResumeFileName { get; set; }
    }
    [MetadataType(typeof(ApplicationMETADATA))]
    public partial class Application { }

    public class AssignmentMETADATA
    {
        [DisplayFormat(NullDisplayText = "N/A")]
        [UIHint("MultilineText")]
        [Display(Name = "Description")]
        public string Description { get; set; }

        [Display(Name = "Active")]
        public bool IsActive { get; set; }
    }
    [MetadataType(typeof(AssignmentMETADATA))]
    public partial class Assignment { }

    public class HeroMETADATA
    {

        [StringLength(25, ErrorMessage = "* Cannot exceed 25 characters")]
        [Display(Name = "Alias")]
        public string Alias { get; set; }

        [Required(ErrorMessage = "* Required")]
        [StringLength(25, ErrorMessage = "* Cannot exceed 25 characters")]
        [Display(Name = "First Name")]
        public string FirstName { get; set; }

        [Required(ErrorMessage = "* Required")]
        [StringLength(25, ErrorMessage = "* Cannot exceed 25 characters")]
        [Display(Name = "Last Name")]
        public string LastName { get; set; }

        [DisplayFormat(NullDisplayText = "N/A")]
        [UIHint("MultilineText")]
        [Display(Name = "Description")]
        public string Description { get; set; }

        [Display(Name = "Active")]
        public bool IsActive { get; set; }

        [Display(Name = "Resume")]
        public string ImageFileName { get; set; }

        [Display(Name = "Amount of Achievements")]
        public int Achievements { get; set; }

    }
    [MetadataType(typeof(HeroMETADATA))]
    public partial class Hero { }

    public class LocationMETADATA
    {
        [StringLength(50, ErrorMessage = "* Cannot exceed 50 characters")]
        public string City { get; set; }

        [StringLength(2, ErrorMessage = "* Enter 2 digit state abbreviation")]
        public string State { get; set; }

        [StringLength(30, ErrorMessage = "* Cannot exceed 30 characters")]
        public string Country { get; set; }
    }
    [MetadataType(typeof(LocationMETADATA))]
    public partial class Location { }

    public class OpenAssignmentMETADATA
    {
        [DisplayFormat(NullDisplayText = "N/A")]
        [UIHint("MultilineText")]
        [Display(Name = "Description")]
        public string Description { get; set; }

    }
    [MetadataType(typeof(OpenAssignmentMETADATA))]
    public partial class OpenAssignment { }

    public class PositionMETADATA
    {
        [Display(Name = "Title")]
        [Required(ErrorMessage = "*Required")]
        [StringLength(50, ErrorMessage = "Title should not exceed 50 characters")]
        public string Title { get; set; }

        [DisplayFormat(NullDisplayText = "N/A")]
        [UIHint("MultilineText")]
        [Display(Name = "Description")]
        public string JobDescription { get; set; }
    }
    [MetadataType(typeof(PositionMETADATA))]
    public partial class Position { }

    public class SkillLevelMETADATA
    {
        [DisplayFormat(DataFormatString = "{0:n0}")]
        [Display(Name = "Prereq")]
        public int SkillValue { get; set; }

        [Display(Name = "Level")]
        [Required(ErrorMessage = "*Required")]
        [StringLength(25, ErrorMessage = "Title should not exceed 25 characters")]
        public string SkillName { get; set; }
    }
    [MetadataType(typeof(SkillLevelMETADATA))]
    public partial class SkillLevel { }

}
